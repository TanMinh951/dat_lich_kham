from django.apps import AppConfig


class ThongBaoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'thong_bao'
