from django.urls import path, include
from . import views
from rest_framework.routers import DefaultRouter
from . import api

router = DefaultRouter()
router.register('', api.UserViewset, basename="userviewset")

urlpatterns = [
    path('api/', include(router.urls)),
    path('login/', views.login, name="login"),
    path('register/', views.register, name="register"),
    path('activate/<uidb64>/<token>/', views.activate, name='activate'),
    path('logout/', views.logout, name="logout"),
    
    #user dash_boad
    path('dash_board/', views.user_dash_board, name='user_dash_board'),
    path('dash_board/change_diachi/', views.user_profile_dia_chi , name = 'user_change_dia_chi'),
    path('dash_board/change_password/', views.user_profile_doi_mat_khau , name = 'user_change_mat_khau'),
    path('lich_kham_benh/', views.user_lich_kham , name = 'user_lich_kham_benh'),
    path('<int:id>/lich_kham_benh/', views.user_lich_kham_detail , name = 'user_lich_kham_benh_detail'),
]

