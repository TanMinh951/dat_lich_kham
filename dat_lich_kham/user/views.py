from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes
from django.core.mail import EmailMessage
from django.contrib.auth.decorators import login_required
from django.contrib.auth.tokens import default_token_generator
from django.conf import settings
from django.contrib import auth
from django.contrib import messages
import requests
from .models import User
from datetime import datetime
from datetime import date
from django.utils import timezone
from django.contrib.auth.hashers import check_password , make_password
from ca_kham.models import CaKham


# Create your views here.

def login(request):
    if request.method == 'POST':
        username = request.POST.get('username', None)
        password = request.POST.get('password',None)
        try:
            user = User.objects.get(username = username)
            if user.check_password(password):
                user = user
            else:
                messages.error(request, 'Tài khoản hoặc mật khẩu không chính xát.')
                return redirect('login')
        except:
            messages.error(request, 'Tài khoản hoặc mật khẩu không chính xát.')
            return redirect('login')
        if user is not None:
            auth.login(request, user)
            url = request.META.get('HTTP_REFERER')
            try:
                query = requests.utils.urlparse(url).query 
                # next=/cart/checkout/
                params = dict(x.split('=') for x in query.split('&'))
                if 'next' in params:
                    nextPage = params['next']
                    return redirect(nextPage)
            except:
                return redirect('user_home')
            return redirect('user_home')
            
        else:
            messages.error(request, 'Tài khoản hoặc mật khẩu không chính xát.')
            return redirect('login')
       
    return render(request, 'user/benh_nhan/user/login.html')

def register(request):
    if request.method == 'POST':
        username = request.POST.get('username', None)
        password = request.POST.get('password',None)
        email = request.POST.get('email',None)

        try:
            user_url = settings.LINK_API + "/user/api/"
            query_string ={
                "email": email,
                "username": username,
                "password": password
            }
            user_response = requests.request("POST", user_url,  json=query_string)
            if user_response.status_code == 400:
                user_resuilt = user_response.json()
                if 'username' in user_resuilt:
                    messages.warning(request, 'Tài khoản đã tồn tại!')
                    return redirect('register')
                if 'email' in user_resuilt:
                     messages.warning(request, 'Email đã tồn tại!')
                     return redirect('register')            
        except:
            messages.warning(request, 'Đã xảy ra lỗi.')
            return redirect('register')
            
        user = User.objects.get(username = username)
          
        current_site = get_current_site(request)
        mail_subject = 'Kích hoạt tài khoản datlichkham online.'
        message = render_to_string('user/benh_nhan/user/verification.html', {
            'user': user,
            'domain': current_site,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'token': default_token_generator.make_token(user)
        })
        send_email = EmailMessage(
            subject= mail_subject,
            body= message,
            from_email= settings.EMAIL_FROM_USER,
            to=[email])
        send_email.send()
        messages.success(request, 'Vui lòng kiếm trả email!')
        return redirect('/user/login/?command=verification&email='+email)
    else:
        return render(request, 'user/benh_nhan/user/register.html')


def activate(request, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = User._default_manager.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = 1
        user.save()
        messages.success(request, 'Chúc mừng bạn đã kích hoạt tài khoản thành công!')
    else:
        messages.error(request, 'Đã có lỗi xay ra!')
    return redirect('login')

def logout(request):
    auth.logout(request)
    return redirect('user_home')


def user_dash_board(request):
    if request.method == "POST":
        ho_ten = request.POST.get("ho_ten", None)
        sdt = request.POST.get("sdt", None)
        email = request.POST.get("email", None)
        ngay_sinh = request.POST.get("ngay_sinh", None)
        gioitinh = request.POST.get("gioi_tinh", None)
        anhdaidien = request.FILES.get("image_anhdaiien", None)
        id = request.user.id
        user_url = settings.LINK_API + "/user/api/"+str(id)+"/"
        data ={
            "hoten": ho_ten,
            "sdt": sdt,
            "ngaysinh": ngay_sinh,
            "email" : email,
            "gioitinh": gioitinh,
        }
        try:
            if anhdaidien is None:
                user_response = requests.request("PATCH", url = user_url,  data=data)
            else:
                user_response = requests.request("PATCH", url = user_url,  data=data, files={'anhdaidien':anhdaidien})
            if user_response.status_code >= 400:
                messages.warning(request, 'Đã có lối xảy ra 1!')
            else:
                user_data =User.objects.get(id = id)
                auth.login(request, user_data)
                messages.success(request, 'Cập nhập tài khoản thành công!')
        except:
            messages.warning(request, 'Đã có lối xảy ra!')
        return redirect('user_dash_board')
    return render(request, 'user/benh_nhan/user/profile_dashboad.html')

def user_profile_dia_chi(request):
    if request.method == "POST":
        id = request.user.id
        tinhthanh = request.POST.get("tinh_thanh", None)
        quanhuyen = request.POST.get("quan_huyen", None)
        phuongxa = request.POST.get("phuong_xa", None)
        diachi  = request.POST.get("dia_chi", None)
        if tinhthanh != '0' and quanhuyen !='0' and phuongxa != '0':
            url_tinh_thanh = 'https://provinces.open-api.vn/api/p/'+tinhthanh+'/'
            tinh_thanh_response = requests.get(url = url_tinh_thanh)
            tinh_thanh_result = tinh_thanh_response.json()
            tinh_thanh = tinh_thanh_result['name']
        
            url_quan_huyen = 'https://provinces.open-api.vn/api/d/'+quanhuyen+'/'
            quan_huyen_response = requests.get(url = url_quan_huyen)
            quan_huyen_result = quan_huyen_response.json()
            quan_huyen = quan_huyen_result['name']
        
            url_phuong_xa = 'https://provinces.open-api.vn/api/w/'+phuongxa+'/'
            phuong_xa_response = requests.get(url = url_phuong_xa)
            phuong_xa_result = phuong_xa_response.json()
            phuong_xa = phuong_xa_result['name']
            
            data = {
                'tinhthanh': tinh_thanh,
                'quanhuyen': quan_huyen,
                'phuongxa': phuong_xa,
                'diachi': diachi
            }
            try:
                user_url = settings.LINK_API + "/user/api/"+str(id)+"/"
                user_response = requests.request("PATCH", url = user_url,  data=data)
                if user_response.status_code > 300:
                    messages.warning(request, 'Đã có lối xảy ra !')
                else:
                    user_data =User.objects.get(id = id)
                    auth.login(request, user_data)
                    messages.success(request, 'Cập nhập tài khoản thành công!')
            except:
                 messages.warning(request, 'Đã có lối xảy ra !')
        else:
            data = {
                'diachi': diachi
            }
            try:
                user_url = settings.LINK_API + "/user/api/"+str(id)+"/"
                user_response = requests.request("PATCH", url = user_url,  data=data)
                if user_response.status_code > 300:
                    messages.warning(request, 'Đã có lối xảy ra !')
                else:
                    user_data =User.objects.get(id = id)
                    auth.login(request, user_data)
                    messages.success(request, 'Cập nhập tài khoản thành công!')
            except:
                messages.warning(request, 'Đã có lối xảy ra !')
        return redirect('user_change_dia_chi')
    return render(request, 'user/benh_nhan/user/profile_dia_chi.html')

def user_profile_doi_mat_khau(request):
    if request.method == "POST":
        id = request.user.id
        mat_khau = request.POST.get('mat_khau', None)
        mat_khau_moi = request.POST.get('mat_khau_moi', None)
        is_corert_pass = check_password(mat_khau, request.user.password)
        if is_corert_pass == True:
            try:
                user_url = settings.LINK_API + "/user/api/"+str(id)+"/"
                data = {
                    "password":mat_khau_moi
                }
                user_response = requests.patch(url = user_url, data = data)
                if user_response.status_code == 200:
                    user_data =User.objects.get(id = id)
                    auth.login(request, user_data)
                    messages.success(request, 'Cập nhập mật khẩu thành công!')
                    return redirect('user_dash_board')
                else:
                    messages.warning(request, 'Cập nhập mật khẩu không thành công!')
                    return render(request, 'user/benh_nhan/user/doi_mat_khau.html')
            except:
                messages.warning(request, 'Đã có lối xảy ra !')
    return render(request, 'user/benh_nhan/user/doi_mat_khau.html')

def user_lich_kham(request):
    id = str(request.user.id)
    lich_kham_url = settings.LINK_API + "lich_kham/api/?ordering=-tinhtrang&tinhtrang=1&tinhtrang=0&user_id="+id
    lich_kham_response = requests.get(url = lich_kham_url)
    lich_kham_result = lich_kham_response.json()
    
    context = {
        'lich_khams': lich_kham_result['results']
    }
    return render(request, 'user/benh_nhan/user/lich_kham_benh.html', context=context)

def user_lich_kham_detail(request, id):
    id = str(id)
    lich_kham_url = settings.LINK_API + "lich_kham/api/"+id+"/"
    lich_kham_response = requests.get(url = lich_kham_url)
    lich_kham_result = lich_kham_response.json()
    context = {
        'lich_kham': lich_kham_result
    }
    return render(request, 'user/benh_nhan/user/xem_ct_lich_kham.html', context=context)
