from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
# Create your models here.

class MyAccountManager(BaseUserManager):
    def create_user(self, username, email, password=None):
        user = self.model(
            email = self.normalize_email(email),
            username = username,
            )
        user.set_password(password)
        user.save()
        return user
    
    def create_superuser(self , username, email, password):
        user = self.create_user(
            email = self.normalize_email(email),
            username=username,
            password=password,
        )
        user.is_admin = True
        user.is_active = True
        user.is_staff = True
        user.is_superadmin = True
        user.save(using =self._db)
        return user
        
class User(AbstractBaseUser):
    username        = models.CharField(max_length=50, unique = True)
    hoten           = models.CharField(max_length = 50,blank=True,null=True)
    email           = models.EmailField(max_length = 50, unique = True)
    sdt             = models.CharField(max_length = 10, blank=True,null=True, unique=True)
    cccd            = models.CharField(max_length = 13, blank=True,null=True, unique=True)
    gioitinh        = models.IntegerField(blank=True, null=True)
    ngaysinh        = models.DateField(blank=True,null=True)
    anhdaidien      = models.ImageField(blank=True, upload_to='user/avartar', default='user/avartar/user.png')
    tinhthanh       = models.CharField(max_length=50, blank=True, null=True)
    quanhuyen       = models.CharField(max_length=50, blank=True, null=True)
    phuongxa        = models.CharField(max_length=50, blank=True, null=True)
    diachi          = models.CharField(max_length=50, blank=True, null=True)
    is_staff        = models.BooleanField(default = False)
    is_superadmin   = models.BooleanField(default = False)
    is_admin        = models.BooleanField(default = False)
    is_active       = models.BooleanField(default = False)
    is_nhanvien     = models.BooleanField(default = False)
    is_bacsi        = models.BooleanField(default = False)
    is_soyte        = models.BooleanField(default = False)
    
    created_at      = models.DateTimeField(auto_now_add=True)
    updated_at      = models.DateTimeField(auto_now=True)
    last_login      = models.DateTimeField(auto_now_add = True)
    
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']
    
    objects = MyAccountManager()
    
    @property 
    def imageURL(self):
        try:
            url = self.anhdaidien.url
        except:
            url = ''
        return url
    
    def __str__(self):
        return self.email

    def has_perm(self, perm, obj = None):
        return self.is_admin
    
    def has_module_perms(self, add_label):
        return True
    
    @property
    def full_address(self):
        return "{0}, {1}, {2}, {3}".format(self.address, self.phuongxa, self.quanhuyen, self.tinhthanh)
    

    
    
    
    
    
        
    
    
    