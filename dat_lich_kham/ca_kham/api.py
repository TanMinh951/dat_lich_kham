from .models import CaKham
from .CaKhamFilter import CaKhamFilter
from rest_framework import status, viewsets
from .serializer import CaKhamSerializer
from rest_framework import filters
from rest_framework.pagination import PageNumberPagination
from datetime import datetime
from rest_framework.response import Response

from rest_framework.decorators import api_view

class CaKhamViewset(viewsets.ModelViewSet):
    queryset = CaKham.objects.all()
    serializer_class = CaKhamSerializer
    filterset_class = CaKhamFilter
    
    def get(self, request, *args, **kwargs):
        
        today = datetime.now().date()
        ca_khams = CaKham.objects.all()
        for ca_kham in ca_khams:
            if ca_kham.tinhtrang =="trong" and today>ca_kham.ngaykham:
                ca_kham.tinhtrang = "quahan"
                ca_kham.save()
        
        return super().get(request, *args, **kwargs)
    
@api_view(['GET'])
def update_tinhtrang_ca_kham(request):
    today = datetime.now().date()
    CaKham.objects.filter(tinhtrang='trong', ngaykham__lt=today).update(tinhtrang='quahan')
    return Response("aaa", status=200)

