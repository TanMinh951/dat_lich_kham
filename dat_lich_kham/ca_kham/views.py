from django.shortcuts import render, redirect
from django.urls import reverse
import requests
from django.http import HttpResponse
from rest_framework import status
from django.conf import settings
from django.contrib import messages
import datetime
import locale
locale.setlocale(locale.LC_TIME, 'vi_VN.utf-8')


# Create your views here.

def get_bac_si(request):
    id = request.user.id
    bac_si_api = settings.LINK_API + "bac_si/api/bac_si?user_id="+ str(id)
    bac_si_response = requests.get(url = bac_si_api)
    bac_si_result = bac_si_response.json()
    bac_si = bac_si_result['results']
    bac_si_id = bac_si[0]['id']
    return bac_si_id

def post_ca_kham(request,ngay_kham, time_start, time_end, gia_kham):
    bac_si_id = get_bac_si(request=request)
    ca_kham_api = settings.LINK_API + "ca_kham/api/"
    data = { 
        "ngaykham": ngay_kham,
        "thoigianbatdau": time_start,
        "thoigiakethuc": time_end,
        "giakham": gia_kham,
        "bacsi": bac_si_id
        }
    ca_kham_response = requests.post(url=ca_kham_api, data=data)
    if ca_kham_response.status_code > 300 :
        return  False
    else:
        return True

def ca_kham_co_san(request):
    time_start = datetime.time(hour=8, minute=30, second=0)
    time_end = datetime.time(hour=9, minute=30, second=0)
    ca_kham_co_san = []
    delta = datetime.timedelta(hours=1)
    time_check = datetime.time(hour=11, minute=30, second=0)
    for i in range(8):
        ca_kham_co_san.append([str(time_start), str(time_end)])
        time_start = (datetime.datetime.combine(datetime.date.today(), time_start) + delta).time()
        time_end = (datetime.datetime.combine(datetime.date.today(), time_end) + delta).time()
        if time_start == time_check:
            time_start = (datetime.datetime.combine(datetime.date.today(), time_start) + delta + delta).time()
            time_end = (datetime.datetime.combine(datetime.date.today(), time_end) + delta + delta).time()
            
    return ca_kham_co_san

def bacsi_ca_kham(request):
    if request.method == "POST":  
        ngay_kham = request.POST.get('ngay_kham_nhanh', None)
        gia = request.POST.get('gia_kham_nhanh', None)
        gia = int(gia)
        time_start = datetime.time(hour=8, minute=30, second=0)
        time_end = datetime.time(hour=9, minute=30, second=0)
        ngay_str = str(ngay_kham)
        ca_khams=[]
        for i in range(8):
            bien = "ca"+ngay_str+str(i+1)
            ca_khams.append(request.POST.get(f'{bien}', None)) 
        for ca_kham in ca_khams:
            if ca_kham is not None:
                check = post_ca_kham(request= request,ngay_kham=ngay_kham, gia_kham=gia, time_start=time_start, time_end=time_end)
                if check == True:
                    messages.success(request, "Đăng kí ca khám thành công")
                else:
                    messages.warning(request, "Đăng kí ca khám thất bại")
            delta = datetime.timedelta(hours=1)
            time_start = (datetime.datetime.combine(datetime.date.today(), time_start) + delta).time()
            time_end = (datetime.datetime.combine(datetime.date.today(), time_end) + delta).time()
            time_check = datetime.time(hour=11, minute=30, second=0)
            if time_start == time_check:
                time_start = (datetime.datetime.combine(datetime.date.today(), time_start) + delta + delta).time()
                time_end = (datetime.datetime.combine(datetime.date.today(), time_end) + delta + delta).time()
        return redirect ('bacsi_ca_kham')
    
    bac_si_id = get_bac_si(request=request)
    bac_si_id = str(bac_si_id)
    ca_kham_api = settings.LINK_API + "ca_kham/api?bacsi_id="+ bac_si_id
    today = datetime.date.today()
    today = today + datetime.timedelta(days=1)
    days = []
    for i in range(7):
        thu = today.weekday() + 2
        days.append([thu,str(today)])
        today = today + datetime.timedelta(days=1)
    ca_khams = {}
    
    for day in days:
        ca_kham_day_api = ca_kham_api + "&ngaykham=" + day[1]
        ca_kham_day_response = requests.get(url=ca_kham_day_api)
        ca_kham_day_result = ca_kham_day_response.json()
        ca_khams[str(day[1])] = ca_kham_day_result['results']
        
    tg_chua_co_ca_kham = {}
    
    for  day ,cac_ca_kham in ca_khams.items():
        ca_kham_da_co = ca_kham_co_san(request)
        dict_day = {}
        tg_dau = []
        tg_sau = []
        for ca_kham in cac_ca_kham:
            tg_dau.append(ca_kham['thoigianbatdau'])
            tg_sau.append(ca_kham['thoigiakethuc'])
        i = 1
        for tg in ca_kham_da_co:
            if len(tg) < 3:
                tg += [None] * (3 - len(tg))
            if tg[0] not in tg_dau or tg[1] not in tg_sau:
                tg[2] = 1
                dict_day[i] = tg 
            else:
                tg[2] = 2
                dict_day[i] = tg 
            i += 1
        tg_chua_co_ca_kham[day] = dict_day
    context = {
        'days': days,
        'ca_khams':ca_khams,
        'today': str(datetime.date.today() + datetime.timedelta(days=1)),
        'tg_chua_co_ca_kham': tg_chua_co_ca_kham
    }
    return render(request, 'user/bac_si/ca_kham/bacsi_ca_kham.html', context=context)

def bacsi_dang_ki_ca_kham(request):
    if request.method == 'POST':
        ngay_kham = request.POST.get('ngay_kham', None)
        gia = request.POST.get('gia_kham', None)
        gia = int(gia)
        gio_bat_dau = request.POST.get('gio_bat_dau', None)
        gio_ket_thuc = request.POST.get('gio_ket_thuc', None)
        check= post_ca_kham(request, ngay_kham=ngay_kham, gia_kham=gia, time_start=gio_bat_dau, time_end=gio_ket_thuc)
        if check == True:
            messages.success(request, "Đăng kí ca khám thành công")
        else:
            messages.warning(request, "Đăng kí ca khám thất bại")
        return redirect ('bacsi_ca_kham')
    return redirect ('bacsi_ca_kham')


def bacsi_xoa_ca_kham(request, id):
    id_ca_kham = str(id)
    ca_kham_api = settings.LINK_API + "ca_kham/api/"+id_ca_kham+"/"
    ca_kham_response = requests.delete(url=ca_kham_api)
    print(ca_kham_response.status_code)
    if ca_kham_response.status_code == 204:
        messages.success(request, "Xóa ca khám thành công")
    else:
        messages.warning(request, "Xóa ca khám thất bại")
    return redirect ('bacsi_ca_kham')


def bacsi_sua_ca_kham(request, id):
    if request.method == "POST":
        id_ca_kham = request.POST.get('id_ca_kham', None)
        bac_si_id = get_bac_si(request=request)
        ngay_kham = request.POST.get('ngay_kham', None)
        gia = request.POST.get('gia_kham', None)
        gia = int(gia)
        gio_bat_dau = request.POST.get('gio_bat_dau', None)
        gio_ket_thuc = request.POST.get('gio_ket_thuc', None)
        data = {
            "ngaykham": ngay_kham,
            "thoigianbatdau": gio_bat_dau,
            "thoigiakethuc": gio_ket_thuc,
            "giakham": gia,
            "bacsi": bac_si_id
        }
        ca_kham_api_update = settings.LINK_API + "ca_kham/api/"+id_ca_kham+"/"
        ca_kham_response_update = requests.patch(url=ca_kham_api_update, data = data)
        
        try:
            if ca_kham_response_update.status_code == 400:
                ca_kham_result_update = ca_kham_response_update.json()
                loi = ca_kham_result_update['non_field_errors']
                messages.warning(request, ca_kham_result_update['non_field_errors'])
                return redirect ('bacsi_ca_kham')
            else:
                messages.success(request, "Cập nhập ca khám thành công")
                return redirect ('bacsi_ca_kham')
        except: 
            messages.warning(request, "Có lỗi xảy ra, vui lòng thử lại sau!")   
            return redirect ('bacsi_ca_kham')
    
    id_ca_kham = str(id)
    ca_kham_api = settings.LINK_API + "ca_kham/api/"+id_ca_kham+"/"
    ca_kham_response = requests.get(url=ca_kham_api)
    ca_kham_result = ca_kham_response.json()
    context = {
        "ca_kham": ca_kham_result
    }
    return render(request, 'user/bac_si/ca_kham/bacsi_sua_ca_kham.html', context=context)

def bacsi_cap_nhap_ca_kham(request):
    if request.method == "POST":
        id_ca_kham = request.POST.get('id_ca_kham', None)
        bac_si_id = get_bac_si(request=request)
        ngay_kham = request.POST.get('ngay_kham', None)
        gia = request.POST.get('gia_kham', None)
        gia = int(gia)
        gio_bat_dau = request.POST.get('gio_bat_dau', None)
        gio_ket_thuc = request.POST.get('gio_ket_thuc', None)
        data = {
            "ngaykham": ngay_kham,
            "thoigianbatdau": gio_bat_dau,
            "thoigiakethuc": gio_ket_thuc,
            "giakham": gia,
            "bacsi": bac_si_id
        }
        ca_kham_api = settings.LINK_API + "ca_kham/api/"+id_ca_kham+"/"
        print(ca_kham_api)
        ca_kham_response = requests.patch(url=ca_kham_api, data = data)
        print(ca_kham_response.status_code)
        if ca_kham_response.status_code == 400:
            messages.warning(request, ca_kham_response.json())
        else:
            ca_kham_result = ca_kham_response.json()
        print(ca_kham_result)
        # ca_kham_result = ca_kham_response.json()
        return redirect ('bacsi_ca_kham')
    return redirect ('bacsi_ca_kham')

    