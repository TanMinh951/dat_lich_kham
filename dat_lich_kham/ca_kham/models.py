from django.db import models
from bac_si.models import BacSi
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from datetime import date

class CaKham(models.Model):
    TINH_TRANG_CHOICES = (
        ('trong', 'Trống'),
        ('danhan', 'Đã nhận lịch'),
        ('lich', 'Có lich'),
        ('dakham', 'Đã khám'),
        ('quahan', 'Quá hạn'),
    )
    bacsi = models.ForeignKey(BacSi, on_delete=models.CASCADE, related_name='ca_khams')
    ngaykham = models.DateField()
    thoigianbatdau = models.TimeField()
    thoigiakethuc = models.TimeField()
    giakham = models.DecimalField(max_digits=10, decimal_places=2)
    tinhtrang = models.CharField(max_length=10, choices=TINH_TRANG_CHOICES, default='trong')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
   
@receiver(post_save, sender=CaKham)
def update_ca_kham_tinhtrang(sender, instance, **kwargs):
    today = timezone.now().date()
    if instance.ngaykham < today and instance.tinhtrang != 'dakham':
        instance.tinhtrang = 'quahan'
        instance.save()