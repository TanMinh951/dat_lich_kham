from .models import CaKham
from rest_framework import serializers
from django.utils.translation import gettext_lazy as _
from bac_si.models import BacSi
from bac_si.serializer import PhongKhamSerializer
from chuyen_khoa.serializer import ChuyenKhoaSerializer
from user.serializer import UserSerializer
from rest_framework import serializers
from ca_kham.models import CaKham
import datetime
from lich_kham.models import LichKham

class LichKhamSerializer2(serializers.ModelSerializer):
    class Meta:
        model = LichKham
        fields = ["id","khamcho","nguoidat","sdt_lien_he","hoten","sdt","ngaysinh","gioitinh","tinhthanh","quanhuyen","phuongxa","diachi","lydokham","donthuoc","ghichubacsi","tinhtrang","cakham","user"]
        
class BacSiSerializer(serializers.ModelSerializer):
    full_info = serializers.SerializerMethodField()
    class Meta:
        model = BacSi
        fields = ['id', 'chucvu', 'hocham', 'hocvi','full_info']
    
    def get_full_info(self, obj):
        chuyenkhoa_serializer = ChuyenKhoaSerializer(obj.chuyenkhoa)
        phongkham_serializer = PhongKhamSerializer(obj.phongkham)
        user_serializer = UserSerializer(obj.user)
        return {
            'user': user_serializer.data,
            'phongkham': phongkham_serializer.data,
            'chuyenkhoa': chuyenkhoa_serializer.data,
        }


class CaKhamSerializer(serializers.ModelSerializer):
    bac_si = serializers.SerializerMethodField()
    thu = serializers.SerializerMethodField()

    class Meta:
        model = CaKham
        fields = ['id', 'ngaykham', 'thoigianbatdau', 'thoigiakethuc','giakham', 'thu','tinhtrang','bacsi','bac_si']
        
    def get_bac_si(self, obj):
        bac_si_serializer = BacSiSerializer(obj.bacsi)
        return {
            'bacsi': bac_si_serializer.data,
        }
        
    def get_thu(self, obj):
        # Xử lý logic để lấy thứ từ ngày
        thu = obj.ngaykham.weekday() + 1  
        return thu
    def validate(self, attrs):
        instance = self.instance
        if self.partial and 'bacsi' not in attrs:
            attrs['bacsi'] = getattr(instance, 'bacsi', None)
            
        if self.partial and 'ngaykham' not in attrs:
            attrs['ngaykham'] = getattr(instance, 'ngaykham', None)
        
        if 'thoigianbatdau' in attrs and 'thoigiakethuc' in attrs:
            if attrs['thoigianbatdau'] >= attrs['thoigiakethuc']:
                raise serializers.ValidationError('Thời gian bắt đầu phải nhỏ hơn thời gian kết thúc')
        else:
            instance = self.instance
            if instance is not None:
                attrs['thoigianbatdau'] = instance.thoigianbatdau
                attrs['thoigiakethuc'] = instance.thoigiakethuc

        queryset = CaKham.objects.exclude(id=self.instance.id) if self.instance else CaKham.objects.all()

        if queryset.filter(
            bacsi=attrs['bacsi'],
            ngaykham=attrs['ngaykham'],
            thoigianbatdau__lt=attrs['thoigiakethuc'],
            thoigiakethuc__gt=attrs['thoigianbatdau']
        ).exists():
            raise serializers.ValidationError('Thời gian khám trùng lên nhau')
        return attrs

