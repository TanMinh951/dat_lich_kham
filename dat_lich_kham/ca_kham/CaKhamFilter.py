import django_filters
from .models import CaKham


class CaKhamFilter(django_filters.FilterSet):
    bacsi_id = django_filters.CharFilter(field_name='bacsi',lookup_expr='exact')
    ngaykham = django_filters.CharFilter(field_name='ngaykham',lookup_expr='exact')
    date_start = django_filters.DateFilter(field_name='ngaykham', lookup_expr='date__gt')
    date_end = django_filters.DateFilter(field_name='ngaykham', lookup_expr='date__lt')
    tinhtrang = django_filters.MultipleChoiceFilter(choices=CaKham.TINH_TRANG_CHOICES)
    class Meta:
        model = CaKham
        fields = ['bacsi_id', 'ngaykham', 'date_start', 'date_end', 'tinhtrang']
