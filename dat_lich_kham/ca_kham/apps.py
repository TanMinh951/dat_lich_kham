from django.apps import AppConfig


class CaKhamConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ca_kham'


