
from django.urls import path, include
from . import views
from . import api
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('api', api.CaKhamViewset, basename="cakhamviewset")
# router.register('phong_kham', api.PhongKhamViewset, basename="phongkhamviewset")
# router.register('file_xat_nhan', api.FileXatNhanViewset, basename="filexatnhaniewset")

urlpatterns = [
    path('', include(router.urls)),
    path('dang_ki_ca_kham/', views.bacsi_ca_kham, name="bacsi_ca_kham"),
    path('bacsi_dang_ki_ca_kham/', views.bacsi_dang_ki_ca_kham, name="bacsi_dang_ki_ca_kham"),
    path('<int:id>/bacsi_xoa_ca_kham/', views.bacsi_xoa_ca_kham, name="bacsi_xoa_ca_kham"),
    path('<int:id>/bacsi_chinh_sua_ca_kham/', views.bacsi_sua_ca_kham , name="bacsi_chinh_sua_ca_kham"),
    path('bacsi_cap_nhap_ca_kham/', views.bacsi_cap_nhap_ca_kham, name="bacsi_cap_nhap_ca_kham"),
    
    
    path('update_tinh_trang_ca_kham', api.update_tinhtrang_ca_kham, name="update_tinh_trang_ca_kham")
    # #path trang bác sĩ
    # path('dang_ky_bac_si/bac_si/', views.bac_si_dangky, name="bacsi_dang_ki_bac_si"),
    # path('dang_ky_cho_duyet/bac_si/', views.bac_si_cho_duyet, name="bacsi_dang_ki_cho_duyet"),
]