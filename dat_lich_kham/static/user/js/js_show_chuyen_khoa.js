const BASE_URL = 'http://127.0.0.1:8000/chuyen_khoa/api/?ordering=id'

const allChuyenKhoa = document.getElementById("all_chuyen_khoa")
const inputSearch = document.getElementById("input-search-bac-si")
const btnSearch = document.getElementById("button-search-bac-si")

var currentPage = 1;
var currentSearch = '';
getBacSi(currentPage)



function getBacSi(page) {
    var url = BASE_URL + '&page=' + page;
    // if (currentSearch !== '') {
    //     url += '&ten=' + currentSearch;
    // }
    $.ajax({
        url: url,
        type: 'GET',
        success: function(response) {
            showChuyenKhoa(response);
            currentPage = page;
        },
        error: function(xhr, status, error) {
            console.log(xhr.responseText);
        }
    });
}

// Hàm tìm kiếm bác sĩ
// function searchBacSi(search) {
//     currentSearch = search;
//     getBacSi(1);
// }



function showChuyenKhoa(response) {
    console.log(response)
    allChuyenKhoa.innerHTML = '';
    const data = response.results
    data.forEach(chuyen_khoa => {
        const movieEl = document.createElement('div');
        movieEl.classList.add('col-lg-4');
        movieEl.classList.add('col-md-6');
        movieEl.innerHTML = `
            <div class="department-block mb-5">
                <a href="${chuyen_khoa.id}/chuyen_khoa_single/"><img src="${chuyen_khoa.anhminhhoa}" alt="" class="img-fluid w-100" style="height:235px;"></a>
                <div class="content" style ="height:175px;">
                    <h4 class="mt-4 mb-2 title-color">${chuyen_khoa.tenkhoa}</h4>
                    <p class="mb-4">${chuyen_khoa.gioithieu}</p>
                </div>
                <a href="${chuyen_khoa.id}/chuyen_khoa_single/" class="read-more btn btn-primary">Xem chi tiết<i class="icofont-simple-right ml-2"></i></a>
            </div>
        `
        allChuyenKhoa.appendChild(movieEl);
    })
    Paging(response.total_pages);
}

// btnSearch.addEventListener("click", (e) => {
//     e.preventDefault()
//     const searchTeam = inputSearch.value
// })

function Paging(totalPage) {
    $("#pagination").twbsPagination({
        totalPages: totalPage,
        visiblePages: 5,
        startPage: currentPage,
        initiateStartPageClick: false,
        onPageClick: function(event, page) {
            getBacSi(page);
        }
    });
}

// // Sự kiện tìm kiếm
// $("#search-form").submit(function(event) {
//     event.preventDefault();
//     var search = $("#search-input").val();
//     searchBacSi(search);
// });