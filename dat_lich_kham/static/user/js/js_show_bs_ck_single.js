const id_khoa = window.location.pathname.split('/')[2];
const URL_API = "http://127.0.0.1:8000/"
const BASE_URL = URL_API + 'bac_si/api/bac_si/?&ordering=id&chuyenkhoa=' + id_khoa
const allBacSi = document.getElementById("all_bac_si")
const link_bs = "http://127.0.0.1:8000/bac_si/"
var currentPage = 1;
var currentSearch = '';
getBacSi(currentPage)

function getBacSi(page) {
    var url = BASE_URL + '&page=' + page;
    $.ajax({
        url: url,
        type: 'GET',
        success: function(response) {
            showBacSi(response);
            currentPage = page;
            console.log(response)
        },
        error: function(xhr, status, error) {
            console.log(xhr.responseText);
        }
    });
}
// Hàm tìm kiếm bác sĩ
function searchBacSi(search) {
    currentSearch = search;
    getBacSi(1);
}



function showBacSi(response) {
    allBacSi.innerHTML = '';
    const data = response.results
    data.forEach(bac_si => {
        const movieEl = document.createElement('div');
        movieEl.classList.add('col-lg-12');
        movieEl.classList.add('shuffle-item');
        movieEl.innerHTML = `
            <div class="ds-bac_si">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class = "tt-bac-si">
                                        <div class="tt-bac-si-img col-lg-3 col-sm-3 col-md-3">
                                            <a href="${link_bs}${bac_si.id}/bac_si_single/"><img src="${bac_si.full_info.user.anhdaidien}" alt="doctor-image" class="img-fluid w-100 "></a>
                                            <a href="${link_bs}${bac_si.id}/bac_si_single/">Xem thêm</a>
                                        </div>
                                        <div class="tt-bac-si-content col-lg-9 col-sm-9 col-md-9">
                                            <a href="${link_bs}${bac_si.id}/bac_si_single/"><strong>${bac_si.full_info.user.hoten}</strong></a>
                                            <br/>
                                            <br>
                                            <p>${bac_si.gioithieu}</p>
                                            <div class="tt-phong-kham">
                                                <h5>ĐỊA CHỈ KHÁM</h5>
                                                <p>
                                                    ${bac_si.full_info.phongkham.tenphongkham}
                                                    <br>
                                                    ${bac_si.full_info.phongkham.diachi}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-lg-6 tt-lich-kham">
                                <div class="tab-container tab-left">
                                    <ul class="nav nav-tabs nav-tabs-classic" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" href="#icon1" data-toggle="tab" role="tab">Thứ 2 - 4/5</a></li>
                                        <li class="nav-item"><a class="nav-link" href="#icon2" data-toggle="tab" role="tab">Thứ 3 - 5/5</span></a></li>
                                        <li class="nav-item"><a class="nav-link" href="#icon3" data-toggle="tab" role="tab">Thứ 4 - 12/12</span></a></li>
                                        <li class="nav-item"><a class="nav-link" href="#icon4" data-toggle="tab" role="tab">Chủ nhật - 12/12</span></a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="icon1" role="tabpanel">
                                            <h4><i class="icofont-calendar"></i>Lịch Khám thứ 2 - 4/5</h4>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                        </div>
                                        <div class="tab-pane" id="icon2" role="tabpanel">
                                            <a href="#" class="btn btn-grey">7:00-7:30</a>
                                        </div>
                                        <div class="tab-pane" id="icon3" role="tabpanel">
                                            <a href="#" class="btn btn-grey">7:00-7:30</a>
                                        </div>
                                        <div class="tab-pane" id="icon4" role="tabpanel">
                                            <a href="#" class="btn btn-grey">7:00-7:30</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `
        allBacSi.appendChild(movieEl);
    })
    if (response.count != "0") {
        Paging(response.total_pages);
    } else {
        const text = document.createElement('div');
        text.classList.add('w-100');
        text.innerHTML = `
        <h4 class="text-center">Hiện không có bác sĩ nào thuộc khoa này!</h4>
        `
        allBacSi.appendChild(text)
    }
}

function Paging(totalPage) {
    $("#pagination").twbsPagination({
        totalPages: totalPage,
        visiblePages: 5,
        startPage: currentPage,
        initiateStartPageClick: false,
        onPageClick: function(event, page) {
            getBacSi(page);
        }
    });
}