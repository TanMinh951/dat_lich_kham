const clinicsDiv = document.getElementById('dk_phong_kham');
// Lấy các phần tử input checkbox
const phongKham = document.getElementById('phong_kham_new');

// Bắt sự kiện khi checkbox Option 1 được tích chọn
phongKham.addEventListener('change', () => {
    if (phongKham.checked) {
        // Nếu checkbox Option 1 được chọn, hiển thị danh sách các phòng khám tương ứng
        clinicsDiv.style.display = 'block';
        document.getElementById('ten_phong_kham').setAttribute("required", true);
        document.getElementById('sdt_phong_kham').setAttribute("required", true);
        document.getElementById('email_phong_kham').setAttribute("required", true);
        document.getElementById('diachi_phong_kham').setAttribute("required", true);
        document.getElementById('gioithieu_phong_kham').setAttribute("required", true);

    } else {
        clinicsDiv.style.display = 'none';
        document.getElementById('ten_phong_kham').removeAttribute("required");
        document.getElementById('sdt_phong_kham').removeAttribute("required");
        document.getElementById('email_phong_kham').removeAttribute("required");
        document.getElementById('diachi_phong_kham').removeAttribute("required");
        document.getElementById('gioithieu_phong_kham').removeAttribute("required");
    }
});