const BASE_URL = 'http://127.0.0.1:8000/bac_si/api/bac_si/?ordering=id'

const allBacSi = document.getElementById("all_bac_si")
const inputSearch = document.getElementById("input-search-bac-si")
const btnSearch = document.getElementById("button-search-bac-si")

var currentPage = 1;
var currentSearch = '';
getBacSi(currentPage)

// function getBacSi(url) {
//     const settings = {
//         "async": true,
//         "crossDomain": true,
//         "url": url,
//         "method": "GET",
//     };
//     $.ajax(settings).done(function(response) {
//         showBacSi(response)
//     });

// }

function getBacSi(page) {
    var url = BASE_URL + '&page=' + page;
    if (currentSearch !== '') {
        url += '&ten=' + currentSearch;
    }
    $.ajax({
        url: url,
        type: 'GET',
        success: function(response) {
            showBacSi(response);
            currentPage = page;
        },
        error: function(xhr, status, error) {
            console.log(xhr.responseText);
        }
    });
}

// Hàm tìm kiếm bác sĩ
function searchBacSi(search) {
    currentSearch = search;
    getBacSi(1);
}

function showBacSi(response) {
    allBacSi.innerHTML = '';
    const data = response.results
    data.forEach(bac_si => {
        const movieEl = document.createElement('div');
        movieEl.classList.add('col-lg-12');
        movieEl.classList.add('shuffle-item');
        movieEl.innerHTML = `
            <div class="ds-bac_si">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class = "tt-bac-si">
                                        <div class="tt-bac-si-img col-lg-3 col-sm-3 col-md-3">
                                            <a href="${bac_si.id}/bac_si_single/"><img src="${bac_si.full_info.user.anhdaidien}" alt="doctor-image" class="img-fluid w-100"></a>
                                            <a href="${bac_si.id}/bac_si_single/">Xem thêm</a>
                                        </div>
                                        <div class="tt-bac-si-content col-lg-9 col-sm-9 col-md-9">
                                            <a href="${bac_si.id}/bac_si_single/"><strong>${bac_si.full_info.user.hoten}</strong></a>
                                            <br/>
                                            <br>
                                            <p>${bac_si.gioithieu}</p>
                                            <div class="tt-phong-kham">
                                                <h5>ĐỊA CHỈ KHÁM</h5>
                                                <p>
                                                    ${bac_si.full_info.phongkham.tenphongkham}
                                                    <br>
                                                    ${bac_si.full_info.phongkham.diachi}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-lg-6 tt-lich-kham">
                                <div class="tab-container tab-left">
                                    <ul class="nav nav-tabs nav-tabs-classic" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" href="#icon1" data-toggle="tab" role="tab">Thứ 2 - 4/5</a></li>
                                        <li class="nav-item"><a class="nav-link" href="#icon2" data-toggle="tab" role="tab">Thứ 3 - 5/5</span></a></li>
                                        <li class="nav-item"><a class="nav-link" href="#icon3" data-toggle="tab" role="tab">Thứ 4 - 12/12</span></a></li>
                                        <li class="nav-item"><a class="nav-link" href="#icon4" data-toggle="tab" role="tab">Chủ nhật - 12/12</span></a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="icon1" role="tabpanel">
                                            <h4><i class="icofont-calendar"></i>Lịch Khám thứ 2 - 4/5</h4>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                            <a href="#" class="btn btn-primary">07:00-07:30</a>
                                        </div>
                                        <div class="tab-pane" id="icon2" role="tabpanel">
                                            <a href="#" class="btn btn-grey">7:00-7:30</a>
                                        </div>
                                        <div class="tab-pane" id="icon3" role="tabpanel">
                                            <a href="#" class="btn btn-grey">7:00-7:30</a>
                                        </div>
                                        <div class="tab-pane" id="icon4" role="tabpanel">
                                            <a href="#" class="btn btn-grey">7:00-7:30</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `
        allBacSi.appendChild(movieEl);
    })
    Paging(response.total_pages);

}

btnSearch.addEventListener("click", (e) => {
    e.preventDefault()
    console.log("aaa")
    const searchTeam = inputSearch.value
    searchBacSi(searchTeam);
})

function Paging(totalPage) {
    $("#pagination").twbsPagination({
        totalPages: totalPage,
        visiblePages: 5,
        startPage: currentPage,
        initiateStartPageClick: false,
        onPageClick: function(event, page) {
            getBacSi(page);
        }
    });
}

// Sự kiện tìm kiếm
$("#search-form").submit(function(event) {
    event.preventDefault();
    var search = $("#input-search-bac-si").val();
    const searchTeam = inputSearch.value
    searchBacSi(search);
});


// function Paging(totalPage) {
//     // $("#pagination").twbsPagination("destroy");
//     var obj = $("#pagination").twbsPagination({
//         totalPages: totalPage,
//         visiblePages: 5,
//         onPageClick: function(event, page) {
//             getBacSi(BASE_URL + "&page=" + page)
//             const searchTeam = inputSearch.value
//             if (searchTeam) {
//                 console.log(searchTeam)
//                 getBacSi(BASE_URL + "&ten=" + searchTeam + "&page=" + page)
//             } else {
//                 getBacSi(BASE_URL + "&page=" + page)
//             }
//         }
//     });

// }