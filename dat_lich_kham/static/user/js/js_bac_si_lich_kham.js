const base_url = 'http://127.0.0.1:8000/';
document.addEventListener("DOMContentLoaded", function() {

    const tenBenhNhan = document.getElementById("ten-benh-nhan")
    const sdt = document.getElementById("sdt")
    const ngaySinh = document.getElementById("ngay-sinh")
    const gioiTinh = document.getElementById("gioi-tinh")
    const diaChi = document.getElementById("dia-chi")
    const lyDoKham = document.getElementById("ly-do-kham")

    const ngayKham = document.getElementById("ngay-kham")
    const gioBatDau = document.getElementById("gio-bat-dau")
    const gioKetThuc = document.getElementById("gio-ket-thuc")
    const giaKham = document.getElementById("gia-kham")
    const noiKham = document.getElementById("noi-kham")
    const diaChiKham = document.getElementById("dia-chi-kham")

    const btnButton = document.getElementById("btn-button")

    var modal = document.getElementById("modal");
    var closeButton = document.getElementsByClassName("close")[0];
    var showDetails = function(event) {
        event.preventDefault();
        var id = this.getAttribute("data-id");
        
        fetch(base_url + `lich_kham/api/${id}/`)
            .then(response => response.json())
            .then(data => {
                var gt;
                if (data['gioitinh'] == 0) {
                    gt = "Nữ";
                } else {
                    gt = "Nam"
                }
                var number = data['ca_kham']['ca_kham']['giakham']
                var amount = parseFloat(number).toFixed(2);

                // Định dạng số thành chuỗi có dấu phân cách hàng nghìn và ký tự đơn vị
                var formattedAmount = parseFloat(amount).toLocaleString('vi-VN', { style: 'currency', currency: 'VND' });
                tenBenhNhan.textContent = data['hoten']
                sdt.textContent = data['sdt']
                ngaySinh.textContent = data['ngaysinh']
                gioiTinh.textContent = gt
                diaChi.textContent = data['diachi']
                lyDoKham.textContent = data['lydokham']
                ngayKham.textContent = data['ca_kham']['ca_kham']['ngaykham']
                gioBatDau.textContent = data['ca_kham']['ca_kham']['thoigianbatdau']
                gioKetThuc.textContent = data['ca_kham']['ca_kham']['thoigiakethuc']
                giaKham.textContent = formattedAmount
                noiKham.textContent = data['ca_kham']['ca_kham']['bac_si']['bacsi']['full_info']['phongkham']['tenphongkham']
                diaChiKham.textContent = data['ca_kham']['ca_kham']['bac_si']['bacsi']['full_info']['phongkham']['diachi']
                btnButton.setAttribute("href",data['id']+'/xac_nhan_lich_kham'   );
            })
            .catch(error => {
                // Xử lý lỗi nếu có
                console.error('Error:', error);
            });
        modal.style.display = "block";
    };

    var showDetailLinks = document.getElementsByClassName("showDetails");
    for (var i = 0; i < showDetailLinks.length; i++) {
        showDetailLinks[i].addEventListener("click", showDetails); 
    }

    closeButton.addEventListener("click", function() {
        modal.style.display = "none";
    });

    window.addEventListener("click", function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    });
    
});
