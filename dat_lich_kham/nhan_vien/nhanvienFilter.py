import django_filters
from .models import NhanVien



class NhanVienFilter(django_filters.FilterSet):
    ten = django_filters.CharFilter(field_name='user.hoten',lookup_expr='icontains')
    class Meta:
        model = NhanVien
        fields = ['ten']
