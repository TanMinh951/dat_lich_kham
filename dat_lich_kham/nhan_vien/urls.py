
from django.urls import path, include
from . import views
from . import api
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('', api.NhanVienViewset, basename="nhanvienviewset")


urlpatterns = [
    path('api/', include(router.urls)),
]