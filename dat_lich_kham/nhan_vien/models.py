from django.db import models
from user.models import User
from bac_si.models import PhongKham


# Create your models here.
class NhanVien(models.Model):
    user       = models.ForeignKey(User, on_delete=models.CASCADE)
    phongkham  = models.ForeignKey(PhongKham, on_delete=models.CASCADE)
    trangthai  = models.BooleanField(default=False)