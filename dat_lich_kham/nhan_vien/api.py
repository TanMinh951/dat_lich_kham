from .models import NhanVien
from .serializer import NhanVienSerializer
from .nhanvienFilter import NhanVienFilter
from rest_framework import filters
from rest_framework import status, viewsets
# Create your views here.

class NhanVienViewset(viewsets.ModelViewSet):
    queryset = NhanVien.objects.all()
    serializer_class = NhanVienSerializer
    filterset_class = NhanVienFilter


    