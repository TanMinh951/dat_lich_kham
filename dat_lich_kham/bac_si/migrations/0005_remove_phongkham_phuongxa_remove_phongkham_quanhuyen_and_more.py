# Generated by Django 4.2.1 on 2023-05-08 14:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bac_si', '0004_alter_bacsi_gioithieu_alter_bacsi_khambenh_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='phongkham',
            name='phuongxa',
        ),
        migrations.RemoveField(
            model_name='phongkham',
            name='quanhuyen',
        ),
        migrations.RemoveField(
            model_name='phongkham',
            name='tinhthanh',
        ),
    ]
