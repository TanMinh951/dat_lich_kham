from .models import PhongKham, BacSi, FileXatNhan
from .BacSiFilter import BacSiFilter, FileXatNhanFilter
from rest_framework import status, viewsets
from .serializer import BacSiSerializer, PhongKhamSerializer, FileXatNhanSerializer
from rest_framework import filters
from rest_framework.pagination import PageNumberPagination
# Create your views here.

class PhongKhamViewset(viewsets.ModelViewSet):
    queryset = PhongKham.objects.all()
    serializer_class = PhongKhamSerializer

class BacSiViewset(viewsets.ModelViewSet):
    queryset = BacSi.objects.all()
    serializer_class = BacSiSerializer
    filterset_class = BacSiFilter
    
    
    
class FileXatNhanViewset(viewsets.ModelViewSet):
    queryset = FileXatNhan.objects.all()
    serializer_class = FileXatNhanSerializer
    filterset_class = FileXatNhanFilter
    