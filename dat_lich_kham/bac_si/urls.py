
from django.urls import path, include
from . import views
from . import api
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('bac_si', api.BacSiViewset, basename="bacsiviewset")
router.register('phong_kham', api.PhongKhamViewset, basename="phongkhamviewset")
router.register('file_xat_nhan', api.FileXatNhanViewset, basename="filexatnhaniewset")

urlpatterns = [
    path('api/', include(router.urls)),
    path('', views.user_doctor, name="user_bac_si"),
    path('<int:id>/bac_si_single/', views.user_doctor_single, name="user_bac_si_single"),
    
    #path trang bác sĩ
    path('dang_ky_bac_si/bac_si/', views.bac_si_dangky, name="bacsi_dang_ki_bac_si"),
    path('dang_ky_cho_duyet/bac_si/', views.bac_si_cho_duyet, name="bacsi_dang_ki_cho_duyet"),
    
    path('ho_so_bac_si/profile', views.bacsi_ho_so_bac_si, name="bacsi_ho_so_bac_si"),
    path('ho_so_bac_si/cap_nhap_ho_so', views.bac_si_cap_nhap_ho_so, name="bacsi_cap_nhap_ho_so"),
    path('ho_so_bac_si/cap_nhap_ho_so_bac_si', views.bac_si_cap_nhap_ho_so_bac_si, name="bacsi_cap_nhap_ho_so_bac_si"),
    path('lich_kham/lich_kham_bac_si', views.bac_si_lich_kham, name="bacsi_lich_kham"),
    
    path('lich_kham/<int:id>/xac_nhan_lich_kham', views.bac_si_xac_nhan_lich_kham, name="bacsi_xac_nhan_lich_kham"),
    path('lich_kham/<int:id>/thuc_hien_kham_benh', views.thuc_hien_kham_benh, name="bacsi_thuc_hien_kham_benh"),
    
    path('lich_su/lich_su_kham_benh', views.bacsi_lich_su_kham_benh, name="bacsi_lich_su_kham_benh"),
]