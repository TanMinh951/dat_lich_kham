from django.db import models
from user.models import User
from chuyen_khoa.models import ChuyenKhoa

# Create your models here.

class PhongKham(models.Model):
    tenphongkham    = models.CharField(max_length=255)
    email           = models.CharField(max_length=255)
    sdt             = models.CharField(max_length=10, blank=True, null=True)
    diachi          = models.CharField(max_length=255, blank=True, null=True)
    logo            = models.ImageField(upload_to='user/bac_si/phong/kham', blank=True, null= True)
    gioithieu       = models.TextField()
    mota            = models.TextField(blank=True, null=True)
    trangthai       = models.BooleanField(default=False)
    
    def __str__(self):
        return self.tenphongkham
    
class BacSi(models.Model):
    user            = models.ForeignKey(User , on_delete=models.CASCADE)
    phongkham       = models.ForeignKey(PhongKham, on_delete=models.CASCADE)
    chucvu          = models.CharField(max_length=255)
    hocham          = models.CharField(max_length=255, blank=True, null=True)
    hocvi           = models.CharField(max_length=255, blank=True, null=True)
    chuyenkhoa      = models.ForeignKey(ChuyenKhoa, on_delete=models.CASCADE)
    gioithieu       = models.TextField(blank=True , null=True)
    thongtinchung   = models.TextField(blank=True, null=True)
    trangthai       = models.BooleanField(default=False)
    
class FileXatNhan(models.Model):
    tenfile     = models.CharField(max_length=255,blank=True, null=True)
    file        = models.FileField(max_length=255, blank=True, null=True)
    bacsi       = models.ForeignKey(BacSi, on_delete=models.CASCADE, related_name='filexatnhan')
    mota        = models.TextField(blank=True, null=True)
    
    def __str__(self):
        return f'{0} , {1}'.format(self.tenfile, self.bacsi.id)
    