from django.contrib import admin

# Register your models here.

from .models import BacSi, PhongKham, FileXatNhan


admin.site.register(PhongKham)

admin.site.register(BacSi)

admin.site.register(FileXatNhan)