from .models import PhongKham, BacSi, FileXatNhan
from chuyen_khoa.serializer import ChuyenKhoaSerializer
from user.serializer import UserSerializer
from user.models import User
from chuyen_khoa.models import ChuyenKhoa
from rest_framework import serializers
from ca_kham.models import CaKham
import datetime

class CaKhamSerializer(serializers.ModelSerializer):
    thu = serializers.SerializerMethodField()
    ngay = serializers.SerializerMethodField()

    class Meta:
        model = CaKham
        fields = ['thu', 'ngay', 'id', 'thoigianbatdau', 'thoigiakethuc', 'giakham', 'ngaykham', 'tinhtrang']
        ordering =['ngay']

    def get_thu(self, obj):
        # Xử lý logic để lấy thứ từ ngày
        thu = obj.ngaykham.weekday() + 1  # Ví dụ: Chủ nhật là 1, Thứ 2 là 2, ..., Thứ 7 là 7
        return thu

    def get_ngay(self, obj):
        # Xử lý logic để lấy ngày/tháng/năm từ ngày
        ngay = obj.ngaykham.strftime('%d-%m')  # Ví dụ: 01/01/2022
        return ngay
        
class FileXatNhanSerializer(serializers.ModelSerializer):
    class Meta:
        model = FileXatNhan
        fields = "__all__"

class PhongKhamSerializer(serializers.ModelSerializer):
    class Meta:
        model = PhongKham 
        fields = "__all__"
        
class BacSiSerializer(serializers.ModelSerializer):
    full_info = serializers.SerializerMethodField()
    # filexatnhan = FileXatNhanSerializer(many = True, read_only = True)
    ca_khams = serializers.SerializerMethodField()
    class Meta:
        model = BacSi
        fields = ['id', 'chucvu', 'hocham', 'hocvi', 'gioithieu', 'thongtinchung','user','chuyenkhoa','phongkham','ca_khams','full_info']
        
    def get_full_info(self, obj):
        chuyenkhoa_serializer = ChuyenKhoaSerializer(obj.chuyenkhoa)
        phongkham_serializer = PhongKhamSerializer(obj.phongkham)
        user_serializer = UserSerializer(obj.user)
        return {
            'user': user_serializer.data,
            'phongkham': phongkham_serializer.data,
            'chuyenkhoa': chuyenkhoa_serializer.data,
        }
        
    def get_ca_khams(self, obj):

        start_date  = datetime.date.today() + datetime.timedelta(days=1)
        end_date = start_date + datetime.timedelta(days=5)
        ca_khams = obj.ca_khams.filter(ngaykham__range=[start_date, end_date])
        ca_khams_serializer = CaKhamSerializer(ca_khams, many=True)
        
        return ca_khams_serializer.data

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        ca_khams = representation['ca_khams']

        # Nhóm các ca khám theo ngày
        grouped_ca_khams = {}
        for ca_kham in ca_khams:
            ngay_kham = ca_kham['ngaykham']
            if ngay_kham in grouped_ca_khams:
                grouped_ca_khams[ngay_kham].append(ca_kham)
            else:
                grouped_ca_khams[ngay_kham] = [ca_kham]

        # Chuyển đổi kết quả nhóm thành list và thêm cột "thứ"
        grouped_ca_khams_list = []
        for ngay_kham, ca_khams in grouped_ca_khams.items():
            # Lấy thứ từ ngày khám
            ngay_kham_dt = datetime.datetime.strptime(ngay_kham, "%Y-%m-%d")
            thu = ngay_kham_dt.strftime("%w")

            # Lấy ngày tháng dưới định dạng "Ngày/Tháng"
            ngay_thang = ngay_kham_dt.strftime("%d-%m") 
            # ngay_kham = ngay_kham.strftime('%d/%m')
            grouped_ca_khams_list.append({
                'ngay_kham': ngay_thang,
                'thu': thu,
                'ca_khams': ca_khams
            })

        representation['ca_khams'] = grouped_ca_khams_list
        return representation
    
    
        
