import django_filters
from .models import BacSi, PhongKham, FileXatNhan



class BacSiFilter(django_filters.FilterSet):
    user_id = django_filters.CharFilter(field_name='user__id',lookup_expr='exact')
    ten = django_filters.CharFilter(field_name='user__hoten',lookup_expr='icontains')
    chuyenkhoa = django_filters.CharFilter(field_name='chuyenkhoa_id',lookup_expr='exact')
    phongkham = django_filters.CharFilter(field_name='phongkham_id',lookup_expr='exact')
    class Meta:
        model = BacSi
        fields = ['ten', 'chuyenkhoa', 'phongkham', 'user_id']
        
        
class FileXatNhanFilter(django_filters.FilterSet):
    bac_si_id = django_filters.NumberFilter(field_name='bacsi',lookup_expr='exact')
    class Meta:
        model = FileXatNhan
        fields = ['bac_si_id']