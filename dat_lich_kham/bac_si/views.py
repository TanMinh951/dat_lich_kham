from django.shortcuts import render, redirect
from django.urls import reverse
import requests
from rest_framework import status
from django.conf import settings
from django.contrib import messages
import datetime 
from urllib.parse import urlparse, parse_qs
import copy
from django.http import HttpRequest
from django.contrib import auth
from .models import User
from datetime import datetime as date_time
# Create your views here.

def user_doctor(request):
    bac_si_url = settings.LINK_API + "/bac_si/api/bac_si/?ordering=id"
    url = request.build_absolute_uri()
    search = ''
    if request.method == "GET":
        search = request.GET.get('search')
    try:
        parsed_url = urlparse(url)
        query_params = parse_qs(parsed_url.query)
        page_number = query_params.get('page', [''])[0]
        search = query_params.get('search', [''])[0]
        if search != '':
            bac_si_url = bac_si_url + "&ten="+ search
        if page_number != '':
            bac_si_url = bac_si_url + "&page=" +page_number
    except:
        pass

    bac_si_response =  requests.request("GET", bac_si_url)
    bac_si_result = bac_si_response.json()
    today = datetime.date.today()
    tomorrow = today + datetime.timedelta(days=1)
    ngay_thang = tomorrow.strftime("%d-%m") 
    
    bac_si_pagi = copy.copy(bac_si_result)
    
    previous_page = bac_si_pagi['previous']
    next_page  = bac_si_pagi['next']
    
    parsed_url_pre = urlparse(previous_page)
    query_params_pre = parse_qs(parsed_url_pre.query)
    page_number_pre = query_params_pre.get('page', [''])[0]
    
    parsed_url_next = urlparse(next_page)
    query_params_next = parse_qs(parsed_url_next.query)
    page_number_next = query_params_next.get('page', [''])[0]
    
    bac_si_pagi['previous'] = page_number_pre
    bac_si_pagi['next'] = page_number_next
    bac_si_pagi['total_pages'] = list(range(1, bac_si_pagi['total_pages'] + 1))
    page_size = [1,2,3]
    context = {
        'search':search,
        'page_size': page_size,
        "pagination": bac_si_pagi,
        "bac_sis": bac_si_result['results'],
        "ngay_thang": ngay_thang,
    }
    return render(request, 'user/benh_nhan/bac_si/bac_si.html', context=context)



def user_doctor_single(request,id):
    id = str(id)
    bac_si_url = settings.LINK_API + "/bac_si/api/bac_si/"+id+"/"
    bac_si_response =  requests.request("GET", bac_si_url)
    bac_si_result = bac_si_response.json()
    today = datetime.date.today()
    tomorrow = today + datetime.timedelta(days=1)
    ngay_thang = tomorrow.strftime("%d-%m") 
    
    bac_si_url_all = settings.LINK_API + "/bac_si/api/bac_si/"
    bac_si_response_all =  requests.request("GET", bac_si_url_all)
    bac_si_result_all = bac_si_response_all.json()

    bac_si_all_tab = []
    bac_si_all_tab = [bac_si_result_all['results'][i:i+4] for i in range(0, len(bac_si_result_all['results']), 4)]
    
    context = {
        "bac_si": bac_si_result,
        "ngay_thang": ngay_thang,
        "bac_si_all_tab": bac_si_all_tab
    }
    
    return render(request,'user/benh_nhan/bac_si/bac_si_single.html', context=context)


def bac_si_dangky(request):
    chuyen_khoa_url = settings.LINK_API + "/chuyen_khoa/api"
    chuyen_khoas = []

    # lấy dữ liệu từ trang đầu tiên
    chuyen_khoa_response = requests.request("GET", chuyen_khoa_url)
    # response = requests.get(url, params={'page_size': page_size})
    chuyen_khoa_result = chuyen_khoa_response.json()
    chuyen_khoas.extend(chuyen_khoa_result['results'])

    # duyệt qua từng trang để lấy tất cả các bản ghi
    while chuyen_khoa_result['next']:
        chuyen_khoa_response = requests.get(chuyen_khoa_result['next'])
        chuyen_khoa_result = chuyen_khoa_response.json()
        chuyen_khoas.extend(chuyen_khoa_result['results'])
    
    phong_khams=[]
    phong_kham_url = settings.LINK_API + "/bac_si/api/phong_kham/"
    phong_kham_response = requests.request("GET", phong_kham_url)
    phong_kham_result = phong_kham_response.json()
    phong_khams = phong_kham_result['results']
    while phong_kham_result['next']:
        phong_kham_response = requests.get(phong_kham_result['next'])
        phong_kham_result = chuyen_khoa_response.json()
        phong_khams.extend(chuyen_khoa_result['results'])
    data = {
            'chuyen_khoas': chuyen_khoas,
            'phong_khams': phong_khams,
        }
    return render(request,'user/bac_si/dang_ky/bacsi_dang_ky_bac_si.html', data)

def bac_si_cho_duyet(request):
    chuc_vu = request.POST.get("chucvu", None)
    chuyen_khoa = request.POST.get("chyenkhoa", None)
    hoc_ham = request.POST.get("hocvi", None)
    if hoc_ham == "1":
        hoc_ham = "Phó giáo sư"
    if hoc_ham == "2":
        hoc_ham = "Giáo sư"
    hoc_vi = request.POST.get("hocvi", None)
    if hoc_vi == "1":
        hoc_vi = "Thạc sĩ"
    if hoc_vi == "2":
        hoc_vi = "Tiến sĩ"
    gioi_thieu = request.POST.get("gioithieu", None)
    thong_tin_chung = request.POST.get("thongtinchung", None)
    phong_kham = request.POST.get("phongkham", None)
    user_id = request.user.id
       
    data = {}
    phong_kham_new_check = request.POST.get("phong_kham_new", None)
    if phong_kham_new_check =='on':
        ten_phong_kham = request.POST.get("ten_phong_kham", None)
        sdt_phong_kham = request.POST.get("sdt_phong_kham", None)
        email_phong_kham = request.POST.get("email_phong_kham", None)
        diachi_phong_kham = request.POST.get("dia_chi_phong_kham", None)
        gioi_thieu_phong_kham = request.POST.get("gioi_thieu_phong_kham", None)
        mo_ta_phong_kham = request.POST.get("mo_ta_phong_kham", None)
        image_phong_kham = request.FILES['image_phong_kham']
        query_string = {
        "tenphongkham": ten_phong_kham,
        "sdt": sdt_phong_kham,   
        "email": email_phong_kham,
        "diachi": diachi_phong_kham,
        "gioithieu": gioi_thieu_phong_kham,
        "mota":mo_ta_phong_kham,
        }
        try:
            phong_kham_url = settings.LINK_API + "bac_si/api/phong_kham/"
            phong_kham_response =  requests.request("POST",url=phong_kham_url,  data=query_string, files={'logo': image_phong_kham})
            phong_kham_resuilt = phong_kham_response.json()
            phong_kham = phong_kham_resuilt['id']
        except:
            data['status'] = 0
            messages.warning(request, "Đăng kí tài bác sĩ khoản lỗi")
            return render(request,'user/bac_si/dang_ky/bacsi_dang_ky_cho_duyet.html', data)
            
    query_string_bac_si ={
        "chucvu": chuc_vu,
        "chuyenkhoa": chuyen_khoa,
        "hocham": hoc_ham,
        "hocvi": hoc_vi,
        "gioithieu":gioi_thieu,
        "thongtinchung":thong_tin_chung,
        "phongkham":phong_kham,
        "user": user_id,
    }
    
    bac_si_url = settings.LINK_API + "bac_si/api/bac_si/"
    bac_si_response =  requests.request("POST", bac_si_url,  json=query_string_bac_si)
    if bac_si_response.status_code >200 & bac_si_response.status_code<300:
        data['status'] = 1
        messages.success(request, "Đăng kí tài khoản bác sĩ thành công")
        user_url = settings.LINK_API + "user/api/" + str(user_id)+"/"
        query_string_user = {
            "is_bacsi" : True
        }
        user_response = requests.request("PATCH", url= user_url, data= query_string_user)
        request.user.is_bacsi = True
    else:
        data['status'] = 0
        messages.warning(request, "Đăng kí tài bác sĩ khoản lỗi")
    return render(request,'user/bac_si/dang_ky/bacsi_dang_ky_cho_duyet.html', data)


def bacsi_ho_so_bac_si(request):
    chuyen_khoa_url = settings.LINK_API + "/chuyen_khoa/api"
    chuyen_khoas = []
    chuyen_khoa_response = requests.request("GET", chuyen_khoa_url)
    chuyen_khoa_result = chuyen_khoa_response.json()
    chuyen_khoas.extend(chuyen_khoa_result['results'])

    while chuyen_khoa_result['next']:
        chuyen_khoa_response = requests.get(chuyen_khoa_result['next'])
        chuyen_khoa_result = chuyen_khoa_response.json()
        chuyen_khoas.extend(chuyen_khoa_result['results'])
    
    phong_khams=[]
    phong_kham_url = settings.LINK_API + "/bac_si/api/phong_kham/"
    phong_kham_response = requests.request("GET", phong_kham_url)
    phong_kham_result = phong_kham_response.json()
    phong_khams = phong_kham_result['results']
    while phong_kham_result['next']:
        phong_kham_response = requests.get(phong_kham_result['next'])
        phong_kham_result = phong_kham_response.json()
        phong_khams.extend(phong_kham_result['results'])
    
    
    user_id = str(request.user.id)
    bac_si_url = settings.LINK_API + "/bac_si/api/bac_si/?user_id="+user_id
    bac_si_response = requests.get(url = bac_si_url)
    bac_si_result = bac_si_response.json()
    bs = bac_si_result['results'][0]

    context = {
        'bac_si': bs,
        'chuyen_khoas': chuyen_khoas,
        'phong_khams': phong_khams
    }

    return render(request,'user/bac_si/ho_so_bac_si/ho_so_bac_si.html', context=context)

def bac_si_cap_nhap_ho_so(request):
     if request.method == "POST":
        ho_ten = request.POST.get("ho_ten", None)
        sdt = request.POST.get("sdt", None)
        email = request.POST.get("email", None)
        ngay_sinh = request.POST.get("ngay_sinh", None)
        gioitinh = request.POST.get("gioi_tinh", None)
        anhdaidien = request.FILES.get("image_anhdaiien", None)
        id = request.user.id
        user_url = settings.LINK_API + "/user/api/"+str(id)+"/"
        data ={
            "hoten": ho_ten,
            "sdt": sdt,
            "ngaysinh": ngay_sinh,
            "email" : email,
            "gioitinh": gioitinh,
        }
        try:
            if anhdaidien is None:
                user_response = requests.request("PATCH", url = user_url,  data=data)
            else:
                user_response = requests.request("PATCH", url = user_url,  data=data, files={'anhdaidien':anhdaidien})
            if user_response.status_code >= 400:
                messages.warning(request, 'Đã có lối xảy ra 1!')
            else:
                user_data =User.objects.get(id = id)
                auth.login(request, user_data)
                messages.success(request, 'Cập nhập tài khoản thành công!')
        except:
            messages.warning(request, 'Đã có lối xảy ra!')
        return redirect('bacsi_ho_so_bac_si')

def bac_si_cap_nhap_ho_so_bac_si(request):
    if request.method == "POST":
        chuc_vu = request.POST.get("chucvu", None)
        chuyen_khoa = request.POST.get("chyenkhoa", None)
        hoc_ham = request.POST.get("hocvi", None)
        if hoc_ham == "1":
            hoc_ham = "Phó giáo sư"
        if hoc_ham == "2":
            hoc_ham = "Giáo sư"
        hoc_vi = request.POST.get("hocvi", None)
        if hoc_vi == "1":
            hoc_vi = "Thạc sĩ"
        if hoc_vi == "2":
            hoc_vi = "Tiến sĩ"
        gioi_thieu = request.POST.get("gioithieu", None)
        thong_tin_chung = request.POST.get("thongtinchung", None)
        user_id = str(request.user.id)
        
        bac_si_url = settings.LINK_API + "/bac_si/api/bac_si/?user_id="+user_id
        bac_si_response = requests.get(url = bac_si_url)
        bac_si_result = bac_si_response.json()
        bs = bac_si_result['results'][0]
        
        query_string_bac_si ={
            "chucvu": chuc_vu,
            "chuyenkhoa": chuyen_khoa,
            "hocham": hoc_ham,
            "hocvi": hoc_vi,
            "gioithieu":gioi_thieu,
            "thongtinchung":thong_tin_chung,
            "user": user_id,
        }
        ho_so_url = settings.LINK_API + "/bac_si/api/bac_si/"+str(bs['id'])+"/"
        user_response = requests.request("PATCH", url= ho_so_url, data= query_string_bac_si)
        if user_response.status_code >= 400:
             messages.warning(request, "Cập nhập tài bác sĩ khoản lỗi")
        else:
            messages.success(request, "Cập nhập tài bác sĩ khoản thành công!")
        return redirect('bacsi_ho_so_bac_si')
    return redirect('bacsi_ho_so_bac_si')


def bac_si_lich_kham(request):
    user_id = str(request.user.id)
    bac_si_url = settings.LINK_API + "/bac_si/api/bac_si/?user_id="+user_id
    bac_si_response = requests.get(url = bac_si_url)
    bac_si_result = bac_si_response.json()
    bac_si_id = bac_si_result['results'][0]['id']
    
    lich_khams=[]
   
    lich_kham_url = settings.LINK_API + "/lich_kham/api/?tinhtrang=0&bac_si_id="+ str(bac_si_id)
    lich_kham_response = requests.request("GET", lich_kham_url)
    lich_kham_result = lich_kham_response.json()
    lich_khams = lich_kham_result['results']
    while lich_kham_result['next']:
        lich_kham_response = requests.get(lich_kham_result['next'])
        lich_kham_result = lich_kham_response.json()
        lich_khams.extend(lich_kham_result['results'])
       
    lich_kham_xn = []
    lich_kham_xn_url = settings.LINK_API + "/lich_kham/api/?tinhtrang=1&bac_si_id="+ str(bac_si_id)
    lich_kham_xn_response = requests.request("GET", lich_kham_xn_url)
    lich_kham_xn_result = lich_kham_xn_response.json()
    lich_kham_xn = lich_kham_xn_result['results']
    while lich_kham_xn_result['next']:
        lich_kham_xn_response = requests.get(lich_kham_xn_result['next'])
        lich_kham_xn_result = lich_kham_xn_response.json()
        lich_kham_xn.extend(lich_kham_xn_result['results'])

    context = {
        'lich_khams' : lich_khams,
        'lich_kham_xn' : lich_kham_xn
    }
    
    return render(request,'user/bac_si/lich_kham/bac_si_lich_kham.html', context=context)

def bac_si_xac_nhan_lich_kham(request, id):
    id = str(id)
    lich_kham_api = settings.LINK_API + "/lich_kham/api/"+id+"/"
    data= {
        "tinhtrang":1
    }
    lich_kham_respone = requests.patch(url = lich_kham_api, data=data)
    lich_kham_result = lich_kham_respone.json()
    id_ca_kham = str(lich_kham_result['cakham'])

    if lich_kham_respone.status_code == 200:
        lich_kham_url = settings.LINK_API + "lich_kham/api/"
        lich_kham_response = requests.post(url= lich_kham_url, data=data)
        lich_kham_result = lich_kham_response.json()
        ca_kham_url = settings.LINK_API + "ca_kham/api/"+id_ca_kham+"/"
        dat_ck={
            "tinhtrang": "danhan",
        }
        ca_kham_response = requests.patch(url= ca_kham_url, data=dat_ck)

        messages.success(request, "Xác nhận lịch khám thành công!")
    else:
        messages.warning(request, "Xác nhận lịch khám thất bại!")
    return redirect('bacsi_lich_kham')

def thuc_hien_kham_benh(request, id):
    id = str(id)
    if request.method == "POST":
        ghi_chu = request.POST.get("ghichu", None)
        don_thuoc = request.POST.get("donthuoc", None)
        data = {
            "ghichubacsi": ghi_chu,
            "donthuoc": don_thuoc,
            "tinhtrang": 2
        }
        lich_kham_url = settings.LINK_API + "/lich_kham/api/"+id+"/"
        lich_kham_response = requests.patch(url= lich_kham_url, data=data)
        lich_kham_result = lich_kham_response.json()
        if lich_kham_response.status_code == 200:
            messages.success(request, "Đã hoàn thành ca khám bệnh!")
        else:
            messages.warning(request, "Đã hoàn thành ca khám bệnh!")
            
        id_ca_kham = str(lich_kham_result['cakham'])
        ca_kham_api = settings.LINK_API + "ca_kham/api/"+id_ca_kham+"/"
        data_ck = {
            "tinhtrang": "dakham"
        }
        ca_kham_response = requests.patch(url=ca_kham_api, data=data_ck)
    
    lich_kham_url = settings.LINK_API + "/lich_kham/api/"+id+"/"
    lich_kham_response = requests.get(url= lich_kham_url)
    lich_kham_result = lich_kham_response.json()
    
    date = lich_kham_result['ngaysinh']
    date_format = '%Y-%m-%d'
    birthday = date_time.strptime(date, date_format)
    current_date = datetime.date.today()
    age = current_date.year - birthday.year
    if current_date.month < birthday.month or (current_date.month == birthday.month and current_date.day < birthday.day):
        age -= 1
    context={
        "age":age,
        "lich_kham": lich_kham_result
    }
    
    return render(request, 'user/bac_si/lich_kham/thuc_hien_kham_benh.html', context=context)

def bacsi_lich_su_kham_benh(request):
    user_id = str(request.user.id)
    bac_si_url = settings.LINK_API + "/bac_si/api/bac_si/?user_id="+user_id
    bac_si_response = requests.get(url = bac_si_url)
    bac_si_result = bac_si_response.json()
    bac_si_id = bac_si_result['results'][0]['id']

    lich_khams = []
    lich_kham_url = settings.LINK_API + "lich_kham/api/?tinhtrang=2&bac_si_id="+ str(bac_si_id)
    lich_kham_response = requests.request("GET", lich_kham_url)
    lich_kham_result = lich_kham_response.json()
    lich_khams = lich_kham_result['results']
    while lich_kham_result['next']:
        lich_kham_response = requests.get(lich_kham_result['next'])
        lich_kham_result = lich_kham_response.json()
        lich_khams.extend(lich_kham_result['results'])

    context = {
        "lich_khams": lich_khams,
    }
    return render(request, 'user/bac_si/ho_so_bac_si/bacsi_lich_su_kham_benh.html', context=context)