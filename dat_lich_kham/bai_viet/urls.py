from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.user_bai_viet, name="user_bai_viet"),
    path('<int:id>/bai_viet_single', views.user_bai_viet_single, name="user_bai_viet_single"),
]