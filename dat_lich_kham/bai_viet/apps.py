from django.apps import AppConfig


class BaiVietConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bai_viet'
