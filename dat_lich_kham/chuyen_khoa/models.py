from django.db import models

# Create your models here.

class ChuyenKhoa(models.Model):
    tenkhoa     = models.CharField(max_length=255)
    gioithieu   = models.TextField(blank=True, null=True, default= True)
    mota        = models.TextField()
    anhminhhoa  = models.ImageField(upload_to='user/bac_si/chuyen_khoa', blank=True, null=True)
    def __str__(self):
        return self.tenkhoa