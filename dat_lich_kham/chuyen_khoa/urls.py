
from django.urls import path, include
from . import views
from . import api
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('', api.ChuyenKhoaViewset, basename="chuyenkhoaviewset")

urlpatterns = [
    path('api/', include(router.urls)),
    path('', views.user_chuyen_khoa, name="user_chuyen_khoa"),
    path('<int:id>/chuyen_khoa_single/', views.user_chuyen_khoa_single, name="user_chuyen_khoa_single"),
    
    ## khác 
    path('gioi_thieu', views.user_gioi_thieu, name="user_gioi_thieu"),
    path('lien_he', views.user_lien_he, name="user_lien_he"),
]