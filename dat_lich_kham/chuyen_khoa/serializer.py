from .models import ChuyenKhoa
from rest_framework import serializers
from rest_framework.pagination import PageNumberPagination
class ChuyenKhoaSerializer(serializers.ModelSerializer):
     class Meta:
         model = ChuyenKhoa
         fields = "__all__"
         pagination_class = PageNumberPagination

        