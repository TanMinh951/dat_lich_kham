import django_filters
from .models import ChuyenKhoa

class chuyenKhoaFilter(django_filters.FilterSet):
    ten_khoa = django_filters.CharFilter(field_name='tenkhoa',lookup_expr='icontains')
    class Meta:
        model = ChuyenKhoa
        fields = ['ten_khoa']