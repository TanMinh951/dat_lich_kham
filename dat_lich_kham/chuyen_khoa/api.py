from .models import ChuyenKhoa
from .chuyenkhoaFilter import chuyenKhoaFilter
from rest_framework import status, viewsets
from .serializer import ChuyenKhoaSerializer
from rest_framework import filters
# Create your views here.


class ChuyenKhoaViewset(viewsets.ModelViewSet):
    queryset = ChuyenKhoa.objects.all().annotate().order_by('id')
    serializer_class = ChuyenKhoaSerializer
    filterset_class = chuyenKhoaFilter
