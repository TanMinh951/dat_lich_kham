from django.shortcuts import render, redirect
from django.conf import settings
from django.contrib import messages
import requests
from urllib.parse import urlparse, parse_qs
import copy
import datetime

# Create your views here.


def user_chuyen_khoa(request):
    chuyen_khoa_url = settings.LINK_API + "/chuyen_khoa/api/"
    chuyen_khoa_response = requests.request("GET", chuyen_khoa_url)
    chuyen_khoa_result = chuyen_khoa_response.json()
    context = {
        'chuyen_khoas': chuyen_khoa_result
    }
    return render(request,'user/benh_nhan/chuyen_khoa/chuyen_khoa.html', context=context)

def user_chuyen_khoa_single(request, id):
    id = str(id)
    chuyen_khoa_url = settings.LINK_API + "/chuyen_khoa/api/"+id+"/"
    chuyen_khoa_response =  requests.request("GET", chuyen_khoa_url)
    chuyen_khoa_result = chuyen_khoa_response.json()
    
    chuyen_khoa_all = []
    chuyen_khoa_all_url = settings.LINK_API + "/chuyen_khoa/api/"
    chuyen_khoa_all_response =  requests.request("GET", chuyen_khoa_all_url)
    chuyen_khoa_all_result = chuyen_khoa_all_response.json()
    chuyen_khoa_all.extend(chuyen_khoa_all_result['results'])
    while chuyen_khoa_all_result['next']:
        chuyen_khoa_all_response = requests.get(chuyen_khoa_all_result['next'])
        chuyen_khoa_all_result = chuyen_khoa_all_response.json()
        chuyen_khoa_all.extend(chuyen_khoa_all_result['results'])
    
    chuyen_khoa_all_tab = []
    chuyen_khoa_all_tab = [chuyen_khoa_all[i:i+4] for i in range(0, len(chuyen_khoa_all), 4)]
    
    
    bac_si_url = settings.LINK_API + "/bac_si/api/bac_si/?chuyenkhoa="+str(id)+"&ordering=id"
    url = request.build_absolute_uri()
    search=''
    try:
        parsed_url = urlparse(url)
        query_params = parse_qs(parsed_url.query)
        page_number = query_params.get('page', [''])[0]
        search = query_params.get('search', [''])[0]
        if search != '':
            bac_si_url = bac_si_url + "&ten="+ search
        if page_number != '':
            bac_si_url = bac_si_url + "&page=" +page_number
    except:
        pass
    
    bac_si_response =  requests.request("GET", bac_si_url)
    bac_si_result = bac_si_response.json()
    today = datetime.date.today()
    tomorrow = today + datetime.timedelta(days=1)
    ngay_thang = tomorrow.strftime("%d-%m") 
    
    bac_si_pagi = copy.copy(bac_si_result)
    
    previous_page = bac_si_pagi['previous']
    next_page  = bac_si_pagi['next']
    
    parsed_url_pre = urlparse(previous_page)
    query_params_pre = parse_qs(parsed_url_pre.query)
    page_number_pre = query_params_pre.get('page', [''])[0]
    
    parsed_url_next = urlparse(next_page)
    query_params_next = parse_qs(parsed_url_next.query)
    page_number_next = query_params_next.get('page', [''])[0]
    
    bac_si_pagi['previous'] = page_number_pre
    bac_si_pagi['next'] = page_number_next
    bac_si_pagi['total_pages'] = list(range(1, bac_si_pagi['total_pages'] + 1))
    page_size = [1,2,3]
    
    
    context = {
        'search':search,
        'page_size': page_size,
        "pagination": bac_si_pagi,
        "bac_sis": bac_si_result['results'],
        "ngay_thang": ngay_thang,
        "id_khoa": id,
        "chuyen_khoa": chuyen_khoa_result,
        "chuyen_khoa_all_tab": chuyen_khoa_all_tab
    }
    return render(request,'user/benh_nhan/chuyen_khoa/chuyen_khoa_single.html', context=context)



## những trang không có cơ sỡ dữ liệu

def user_gioi_thieu(request):
    chuyen_khoa_all = []
    chuyen_khoa_all_url = settings.LINK_API + "/chuyen_khoa/api/"
    chuyen_khoa_all_response =  requests.request("GET", chuyen_khoa_all_url)
    chuyen_khoa_all_result = chuyen_khoa_all_response.json()
    chuyen_khoa_all.extend(chuyen_khoa_all_result['results'])
    while chuyen_khoa_all_result['next']:
        chuyen_khoa_all_response = requests.get(chuyen_khoa_all_result['next'])
        chuyen_khoa_all_result = chuyen_khoa_all_response.json()
        chuyen_khoa_all.extend(chuyen_khoa_all_result['results'])
    
    chuyen_khoa_all_tab = []
    chuyen_khoa_all_tab = [chuyen_khoa_all[i:i+4] for i in range(0, len(chuyen_khoa_all), 4)]
    context = {
        "chuyen_khoa_all_tab": chuyen_khoa_all_tab
    }
    return render(request,'user/benh_nhan/gioi_thieu/gioi_thieu.html', context=context)

def user_lien_he(request):
    return render(request,'user/benh_nhan/gioi_thieu/lien_he.html')
