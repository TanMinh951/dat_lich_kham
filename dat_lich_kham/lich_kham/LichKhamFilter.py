import django_filters
from .models import LichKham




class LicKhamFilter(django_filters.FilterSet):
    tinh_trang = django_filters.CharFilter(field_name='tinhtrang',lookup_expr='exact')
    user_id = django_filters.NumberFilter(field_name='user_id',lookup_expr='exact')
    bac_si_id = django_filters.NumberFilter(field_name='cakham__bacsi',lookup_expr='exact')
    bac_si_xn= django_filters.CharFilter(field_name='cakham__tinhtrang',lookup_expr='exact')
    tinhtrang = django_filters.MultipleChoiceFilter(choices=LichKham.TRANG_THAI_CHOICES)
    class Meta:
        model = LichKham
        fields = ['tinh_trang', 'user_id', 'tinhtrang']