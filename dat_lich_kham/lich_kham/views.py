from django.shortcuts import render, HttpResponse, redirect
from rest_framework import status
from django.conf import settings
import requests
from datetime import datetime
from django.contrib import messages
# Create your views here.

def user_dat_lich_kham(request, id):
    id = str(id)
    ca_kham_url = settings.LINK_API + "ca_kham/api/"+id+'/'
    ca_kham_response =requests.get(url=ca_kham_url)
    ca_kham_result = ca_kham_response.json()
    ca_kham_result['thoigianbatdau'] = datetime.strptime(ca_kham_result['thoigianbatdau'], "%H:%M:%S").strftime("%H:%M")
    ca_kham_result['thoigiakethuc'] = datetime.strptime(ca_kham_result['thoigiakethuc'], "%H:%M:%S").strftime("%H:%M")
    ca_kham_result['ngaykham']= datetime.strptime(ca_kham_result['ngaykham'], "%Y-%m-%d").strftime("%d/%m/%Y")
    ca_kham_result['giakham'] = f"{float(ca_kham_result['giakham']):,.0f}".replace(",", ".")
    context = {
        'ca_kham': ca_kham_result
    }
    return render(request, 'user/benh_nhan/lich_kham/dat_lich_kham.html', context=context)


def user_da_dat_lich(request):
    if request.method == "POST":

        if request.user.id == None:
            messages.warning(request, "Vui lòng đăng nhập vào hệ thống trước!")
            return redirect('login')
        id_ca_kham = request.POST.get('id_ca_kham', None)
        ng_dat = request.POST.get('ho_ten_nguoi_dat', None)
        sdt_dat = request.POST.get('sdt_lien_he', None)
        hoten = request.POST.get('ho_ten', None)
        gioitinh = request.POST.get('gioitinh', None)
        sdt = request.POST.get('sdt', None)
        ngaysinh = request.POST.get('ngaysinh', None)
        
        tinhthanh = request.POST.get("tinh_thanh", None)
        quanhuyen = request.POST.get("quan_huyen", None)
        phuongxa = request.POST.get("phuong_xa", None)
        diachi  = request.POST.get("dia_chi", None)
        lydokham  = request.POST.get("ly_do_kham", None)
        if tinhthanh != '0' and quanhuyen !='0' and phuongxa != '0':
            url_tinh_thanh = 'https://provinces.open-api.vn/api/p/'+tinhthanh+'/'
            tinh_thanh_response = requests.get(url = url_tinh_thanh)
            tinh_thanh_result = tinh_thanh_response.json()
            tinh_thanh = tinh_thanh_result['name']
        
            url_quan_huyen = 'https://provinces.open-api.vn/api/d/'+quanhuyen+'/'
            quan_huyen_response = requests.get(url = url_quan_huyen)
            quan_huyen_result = quan_huyen_response.json()
            quan_huyen = quan_huyen_result['name']
        
            url_phuong_xa = 'https://provinces.open-api.vn/api/w/'+phuongxa+'/'
            phuong_xa_response = requests.get(url = url_phuong_xa)
            phuong_xa_result = phuong_xa_response.json()
            phuong_xa = phuong_xa_result['name']
        else:
            tinh_thanh= request.user.tinhthanh
            quan_huyen = request.user.quanhuyen 
            phuong_xa = request.user.phuongxa
           
        data ={
            "cakham": id_ca_kham,
            'user' :request.user.id,
            'nguoidat':ng_dat,
            "sdt_lien_he": sdt_dat,
            "khamcho": 0,
            'hoten':hoten,
            'sdt':sdt,
            'cccd':"",
            'ngaysinh':ngaysinh,
            'gioitinh':gioitinh,
            'tinhthanh':tinh_thanh,
            'quanhuyen':quan_huyen,
            'phuongxa':phuong_xa,
            'diachi':diachi,
            'lydokham':lydokham  
        }
        if ng_dat != "":
            data['khamcho']= 1,
            data['nguoidat']= request.user.hoten
        try:  
            lich_kham_url = settings.LINK_API + "lich_kham/api/"
            lich_kham_response = requests.post(url= lich_kham_url, data=data)
            lich_kham_result = lich_kham_response.json()
            ca_kham_url = settings.LINK_API + "ca_kham/api/"+id_ca_kham+"/"
            dat_ck={
                "tinhtrang": "lich",
            }
            ca_kham_response = requests.patch(url= ca_kham_url, data=dat_ck)
            ca_kham_result = ca_kham_response.json()
            messages.success(request, "Đăng kí ca khám thành công")
        except:
            messages.warning(request, "Đăng kí ca khám khoản lỗi")
        context = {
            'lich_kham' : lich_kham_result,
            'ca_kham': ca_kham_result,
        }
    
        return render(request, 'user/benh_nhan/lich_kham/dat_lich_thanh_cong.html', context)
    return redirect("user_bac_si")


