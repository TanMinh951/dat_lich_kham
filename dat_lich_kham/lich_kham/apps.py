from django.apps import AppConfig


class LichKhamConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lich_kham'
