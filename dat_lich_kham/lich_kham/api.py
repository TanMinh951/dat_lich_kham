from .models import LichKham
from .serializer import LichKhamSerializer
from rest_framework import status, viewsets
from .LichKhamFilter import LicKhamFilter

class LichKhamViewset(viewsets.ModelViewSet):
    queryset = LichKham.objects.all()
    serializer_class = LichKhamSerializer
    filterset_class = LicKhamFilter

    