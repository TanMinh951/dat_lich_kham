from .models import LichKham
from rest_framework import serializers
from ca_kham.serializer import CaKhamSerializer
from datetime import datetime
from itertools import groupby

class LichKhamSerializer(serializers.ModelSerializer):
    ca_kham = serializers.SerializerMethodField()
    class Meta:
        model = LichKham
        fields = ["id","khamcho","nguoidat","sdt_lien_he","hoten","sdt","ngaysinh","gioitinh","tinhthanh","quanhuyen","phuongxa","diachi","lydokham","donthuoc","ghichubacsi","tinhtrang","cakham","user",'ca_kham']
        
    def get_ca_kham(self, obj):
        ca_kham_serializer = CaKhamSerializer(obj.cakham)
        return {
            'ca_kham': ca_kham_serializer.data,
        }
        
        