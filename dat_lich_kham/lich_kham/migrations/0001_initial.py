# Generated by Django 4.2.1 on 2023-05-19 13:59

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('ca_kham', '0004_remove_cakham_bacsi_kham_trung_alter_cakham_bacsi'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='LichKham',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('khamcho', models.IntegerField(default=1)),
                ('hoten', models.CharField(blank=True, max_length=255, null=True)),
                ('sdt', models.CharField(blank=True, max_length=10, null=True, unique=True)),
                ('cccd', models.CharField(blank=True, max_length=13, null=True, unique=True)),
                ('ngaysinh', models.DateField(blank=True, null=True)),
                ('gioitinh', models.IntegerField(default=2)),
                ('tinhthanh', models.CharField(blank=True, max_length=255, null=True)),
                ('quanhuyen', models.CharField(blank=True, max_length=255, null=True)),
                ('phuongxa', models.CharField(blank=True, max_length=255, null=True)),
                ('diachi', models.CharField(blank=True, max_length=255, null=True)),
                ('lydokham', models.TextField(blank=True, null=True)),
                ('tinhtrang', models.IntegerField(default=1)),
                ('cakham', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ca_kham.cakham')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
