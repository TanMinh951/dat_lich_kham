
from django.urls import path, include
from . import views
from . import api
from rest_framework.routers import DefaultRouter
from . import views
from . import api

router = DefaultRouter()
router.register('api', api.LichKhamViewset, basename="lichkhamviewset")

# router.register('phong_kham', api.PhongKhamViewset, basename="phongkhamviewset")
# router.register('file_xat_nhan', api.FileXatNhanViewset, basename="filexatnhaniewset")
urlpatterns = [
    path('', include(router.urls)),
    path('<int:id>/dat_lich_kham', views.user_dat_lich_kham , name="user_dat_lich_kham"),
    path('da_dat_lich_kham', views.user_da_dat_lich , name="user_da_dat_lich_kham"),
]