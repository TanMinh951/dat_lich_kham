from django.db import models
from ca_kham.models import CaKham
from user.models import User
# Create your models here.



class LichKham(models.Model):
    TRANG_THAI_CHOICES = (
        (0, 0),
        (1, 1),
        (2, 2),
    )
    cakham = models.ForeignKey(CaKham, on_delete=models.CASCADE, related_name='lich_kham')
    user  = models.ForeignKey(User, on_delete=models.CASCADE)
    khamcho = models.IntegerField(default=1)
    nguoidat = models.CharField(max_length=255, null=True, blank=True)
    sdt_lien_he   = models.CharField(max_length = 10, blank=True,null=True)
    hoten = models.CharField(max_length=255, null=True, blank=True)
    sdt   = models.CharField(max_length = 10, blank=True,null=True)
    ngaysinh = models.DateField(blank=True, null=True)
    gioitinh = models.IntegerField(default=2)
    tinhthanh = models.CharField(max_length=255, blank= True, null= True)
    quanhuyen = models.CharField(max_length=255, blank= True, null= True)
    phuongxa = models.CharField(max_length=255, blank= True, null= True)
    diachi = models.CharField(max_length=255, blank= True, null= True)
    lydokham = models.TextField(blank=True, null=True)
    ghichubacsi = models.TextField(blank=True, null=True)
    donthuoc = models.TextField(blank=True, null=True)
    tinhtrang = models.IntegerField(default=0, choices=TRANG_THAI_CHOICES)
    