from django.shortcuts import render, redirect
from django.urls import reverse
import requests
from django.http import HttpResponse
from rest_framework import status
from django.conf import settings
from django.contrib import messages

# Create your views here.


def home_admin(request):
    return render(request, 'admin_ad/index.html')

## Bệnh Nhân
def quan_ly_benh_nhan(request):
    return render(request, 'admin_ad/quan_ly_benh_nhan/quan_ly_benh_nhan.html')

def add_benh_nhan(request):
    return render(request, 'admin_ad/quan_ly_benh_nhan/add_benh_nhan.html')


## Bác sĩ
def quan_ly_bac_si(request):
    return render(request, 'admin_ad/quan_ly_bac_si/quan_ly_bac_si.html')


## Phòng Khám
def quan_ly_phong_kham(request):
    return render(request, 'admin_ad/quan_ly_phong_kham/quan_ly_phong_kham.html')


## Nhân Viên
def quan_ly_nhan_vien(request):
    return render(request, 'admin_ad/quan_ly_nhan_vien/quan_ly_nhan_vien.html')

## Dịch vụ Khám
def quan_ly_dich_vu_kham(request):
    return render(request, 'admin_ad/quan_ly_dich_vu_kham/quan_ly_dich_vu_kham.html')


## Chuyên Khoa
def quan_ly_chuyen_khoa(request):
    chuyen_khoa_url = settings.LINK_API + "/chuyen_khoa/api/"
    chuyen_khoas = []
    chuyen_khoa_response = requests.request("GET", chuyen_khoa_url)
    chuyen_khoa_result = chuyen_khoa_response.json()
    chuyen_khoas.extend(chuyen_khoa_result['results'])
    while chuyen_khoa_result['next']:
        chuyen_khoa_response = requests.get(chuyen_khoa_result['next'])
        chuyen_khoa_result = chuyen_khoa_response.json()
        chuyen_khoas.extend(chuyen_khoa_result['results'])
    context = {
        "chuyen_khoas": chuyen_khoas
    }
    # if chuyen_khoa_response.status_code == 200:
    #     messages.success(request, "Thêm chuyên khoa thành công")
    #     return redirect(reverse('admin_quan_ly_chuyen_khoa'))
    # else:
    #     messages.warning(request, "Thêm chuyên khoa thất bại")
    #     return redirect(reverse('quan_ly_chuyen_khoa'))
    return render(request, 'admin_ad/quan_ly_chuyen_khoa/quan_ly_chuyen_khoa.html', context=context)


def add_chuyen_khoa(request):
    if request.method == "POST":
        tenkhoa = request.POST.get('ten_khoa', None)
        mota = request.POST.get('mo_ta_chuyen_khoa',None)
        giôithieu = request.POST.get('gioi_thieu_chuyen_khoa',None)
        image_chuyen_khoa = request.FILES['image_khoa']
        
        url_chuyen_khoa = settings.LINK_API + "chuyen_khoa/api/"
        data = {
            "tenkhoa": tenkhoa,
            "gioithieu": giôithieu,
            "mota": mota,
        }
        context = {}
        chuyen_khoa_response =  requests.request("POST",url=url_chuyen_khoa,  data=data, files={'anhminhhoa': image_chuyen_khoa})
        chuyen_khoa_result = chuyen_khoa_response.json()
        if chuyen_khoa_response.status_code == 201:
            messages.success(request, "Thêm chuyên khoa thành công")
            return redirect(reverse('admin_quan_ly_chuyen_khoa'))
        else:
            messages.warning(request, "Thêm chuyên khoa thất bại")
            return redirect(reverse('quan_ly_chuyen_khoa'))
    return render(request, 'admin_ad/quan_ly_chuyen_khoa/add_chuyen_khoa.html')
def detail_chuyen_khoa(request,chuyen_khoa_id):
    return render(request, 'admin_ad/quan_ly_chuyen_khoa/detail_chuyen_khoa.html')


## Lịch khám 
def quan_ly_lich_kham(request):
    return render(request, 'admin_ad/quan_ly_lich_kham/quan_ly_lich_kham.html')


## Lịch khám 
def quan_ly_ca_kham(request):
    return render(request, 'admin_ad/quan_ly_ca_kham/quan_ly_ca_kham.html')