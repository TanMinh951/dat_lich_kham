
from django.urls import path, include
from . import views


urlpatterns = [
    path('', views.home_admin, name="admin_home_admin"),
    path('quan_ly_benh_nhan/', views.quan_ly_benh_nhan, name="admin_quan_ly_benh_nhan"),
    path('add_benh_nhan/', views.add_benh_nhan, name="admin_add_benh_nhan"),
    path('quan_ly_bac_si/', views.quan_ly_bac_si, name="admin_quan_ly_bac_si"),
    path('quan_ly_phong_kham/', views.quan_ly_phong_kham, name="admin_quan_ly_phong_kham"),
    path('quan_ly_dich_vu_kham/', views.quan_ly_dich_vu_kham, name="admin_quan_ly_dich_vu_kham"),
    path('quan_ly_nhan_vien/', views.quan_ly_nhan_vien, name="admin_quan_ly_nhan_vien"),
    
    ## Chuyên khoa
    path('quan_ly_chyen_khoa/', views.quan_ly_chuyen_khoa, name="admin_quan_ly_chuyen_khoa"),
    path('quan_ly_chyen_khoa/add_chuyen_khoa/', views.add_chuyen_khoa, name="admin_add_chuyen_khoa"),
    path('quan_ly_chyen_khoa/<int:chuyen_khoa_id>/detail_chuyen_khoa/', views.detail_chuyen_khoa, name="admin_detail_chuyen_khoa"),
    
    ## Ca khám 
    path('quan_ly_ca_kham/', views.quan_ly_ca_kham, name="admin_quan_ly_ca_kham"),
    
    ## Lịch khám
    path('quan_ly_lich_kham/', views.quan_ly_lich_kham, name="admin_quan_ly_lich_kham"),
]