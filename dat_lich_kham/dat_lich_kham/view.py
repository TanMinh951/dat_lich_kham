from django.shortcuts import render
import requests
from django.conf import settings
# Create your views here.
from datetime import datetime
from ca_kham.models import CaKham

def home(request):

    ca_kham_update_url =settings.LINK_API + "ca_kham/update_tinh_trang_ca_kham"
    requests.get(url=ca_kham_update_url)
    
    chuyen_khoa_all = []
    chuyen_khoa_all_url = settings.LINK_API + "/chuyen_khoa/api/"
    chuyen_khoa_all_response =  requests.request("GET", chuyen_khoa_all_url)
    chuyen_khoa_all_result = chuyen_khoa_all_response.json()
    chuyen_khoas = chuyen_khoa_all_result['results']
    chuyen_khoa_all.extend(chuyen_khoa_all_result['results'])
    
    chuyen_khoa_all_tab = []
    chuyen_khoa_all_tab = [chuyen_khoa_all[i:i+4] for i in range(0, len(chuyen_khoa_all), 4)]
    context = {
        "chuyen_khoas":chuyen_khoas,
        "chuyen_khoa_all_tab": chuyen_khoa_all_tab
    }
    return render(request, 'user/index.html', context=context)

