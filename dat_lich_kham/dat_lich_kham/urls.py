"""
URL configuration for dat_lich_kham project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from . import view

urlpatterns = [
    path('', view.home, name="user_home"),
    path('admin_user/',include('admin_user.urls')),
    path('user/',include('user.urls')),
    path('ca_kham/',include('ca_kham.urls')),
    path('lich_kham/',include('lich_kham.urls')),
    path('thong_bao/',include('thong_bao.urls')),
    path('bac_si/',include('bac_si.urls')),
    path('chuyen_khoa/',include('chuyen_khoa.urls')),
    path('bai_viet/',include('bai_viet.urls')),
    path('nhan_vien/',include('nhan_vien.urls')),
    path('admin/', admin.site.urls),
]+ static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)


